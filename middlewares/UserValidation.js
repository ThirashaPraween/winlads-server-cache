const jwt = require ('jsonwebtoken')
//PASS MONGODB ID Here and this generates and JWT token for that
 const createToken = ({_id}) => {
    return jwt.sign({_id}, process.env.SECRET_KEY,{expiresIn:'1d'});
}

 const verifyToken = (token) => {
    return jwt.verify(token, process.env.SECRET_KEY);
}


const LoginValidator = (req, res, next) => {
    // Access the Authorization header
    const authorizationHeader = req.headers['authorization'];
  
    if (authorizationHeader) {
      
      const token = authorizationHeader.split(' ')[1];
      //console.log('Token:', token);
      const id = verifyToken(token);
      //console.log(id);

      // Pass the token to the next middleware or route handler

      //Uncomment this if need to verify what user is sending the request
      req.loggedInId = id;
    if(id){

    }
      next();
    } else {
      // Handle the case where the Authorization header is missing
      res.status(401).json({ message: 'Unauthorized: Token is Missing, Please Login Again' });
    }
  }
  
  module.exports = {
    LoginValidator,
    createToken,
    verifyToken,
  };;