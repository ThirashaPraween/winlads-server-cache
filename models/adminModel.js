const mongoose = require("mongoose");

const AdminSchema = new mongoose.Schema({
  name: String,
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  mobile: Number,
  passport: Number,
  //   tin: Number,
  dob: Date,
  address: String,
  //   refferalId: String,
  //   uid: String,
  //   stripe_id: String,
  //   sub_id: String,
  //   subscriptionid:String,
  //   fcmtoken:String,
  //   image: String,
  //   balance: { type: Number, default: 0.00 }
  createdat: { type: Date, default: Date.now() },
});

const AdminModal = mongoose.model("admins", AdminSchema);
module.exports = AdminModal;
