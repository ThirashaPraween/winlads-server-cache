const mongoose = require("mongoose");

const RafflesRoundsSchema = new mongoose.Schema({
  name: String,
  roundimage: String,
  startingtime: Date,
  endtime: Date,
  desc: String,
  raffleid: String,
  price: Number,
  lastno: Number,
  youtubeLink: String,
  winningNumber: String,
  roundStatus: { type: Number, default: 1 },
  status: { type: Number, default: 1 },
  createdat: { type: Date, default: Date.now() },
});

const RaffleRoundsModal = mongoose.model("rounds", RafflesRoundsSchema);
module.exports = RaffleRoundsModal;
