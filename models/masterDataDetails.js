const mongoose = require("mongoose");

const masterDataDetailsSchema = new mongoose.Schema({
  categoryName: String,
  categoryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'masterDataCategory'
  },
  name: String,
  Description: String,
  sortID: Number,
  detailsStatus: Number,
  view: Number,
  createdat: { type: Date, default: Date.now() },
});

const MasterDataDetailsModel = mongoose.model("masterDataDetails", masterDataDetailsSchema);
module.exports = MasterDataDetailsModel;
