const mongoose = require("mongoose");

const BalanceTransferSchema = new mongoose.Schema({
  uid: String,
  method: String,
  bank: String,
  accountnumber: String,
  name: String,
  purpose: String,
  amount: Number,
  status: String,
  bnb: String,
  createdat: { type: Date, default: Date.now() },
});

const BalanceTransferModel = mongoose.model("balancetransfer", BalanceTransferSchema);
module.exports = BalanceTransferModel;
