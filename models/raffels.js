const mongoose = require("mongoose");

const RafflesSchema = new mongoose.Schema({
  name: String,
  date: String,
  type: String,
  image: String,
  raffleimage: String,
  color: String,
  status: { type: Number, default: 1 },
  createdat: { type: Date, default: Date.now() },
});

const RafflesModal = mongoose.model("raffles", RafflesSchema);
module.exports = RafflesModal;
