const { ObjectId } = require("mongodb");
const mongoose = require("mongoose");

const UserRafflesSchema = new mongoose.Schema({
    uid: String,
    raffleroundid: ObjectId,
    subid: String,
    type: String,
    createddate: Date,
    entryNumber: String,
    status: { type: Number, default: 1 },
    createdat: { type: Date, default: Date.now() },
});

const UserRafflesModel = mongoose.model("userraffles", UserRafflesSchema);
module.exports = UserRafflesModel;
