const mongoose = require("mongoose");

const SubSchema = new mongoose.Schema({
    name: String,
    desc: Array,
    color: String,
    colorFrom:String,
    subid: String,
    subidsemiannual: String,
    subidannual: String,
    monthly: Number,
    semiannualy: Number,
    annualy: Number,
    price_id:String,
    price_id_semiannual:String,
    price_id_annual:String,
    raffle_count:Number,
    raffle_count_semiannual:Number,
    raffle_count_annual:Number,
    refferal_limit:Number,
    refferal_commision_l1:Number,
    refferal_commision_l2:Number,
    refferal_commision_l3:Number,
    refferal_commision_l4:Number,
    withdrawal_limit:Number,
    withdrawal_timeframe:Number,
    withdrawal_timeframe_term:String,
    chances:Number,
});

const SubsciptionPlansModel = mongoose.model("subscriptionplans", SubSchema);
module.exports = SubsciptionPlansModel;
