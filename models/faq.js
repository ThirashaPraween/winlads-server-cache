const mongoose = require("mongoose");

const FaqSchema = new mongoose.Schema({
  q: String,
  a: String,
  status: { type: Number, default: 1 },
  createdat: { type: Date, default: Date.now() },
});

const FaqModel = mongoose.model("faq", FaqSchema);
module.exports = FaqModel;
