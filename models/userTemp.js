const mongoose = require("mongoose");

const UserTempSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  email: String,
  mobile: String,
  nic: String,
  licencse: String,
  tin: Number,
  dob: Date,
  address: String,
  address2: String,
  city: String,
  state: String,
  postalcode: String,
  refferalId: String,
  uid: String,
  stripe_id: String,
  sub_id: String,
  subscriptionid:String,
  fcmtoken:String,
  image: String,
  password:String,
  balance: { type: Number, default: 0.00 },
  createdat: { type: Date, default: Date.now() }
});

const UserTempModal = mongoose.model("usertemp", UserTempSchema);
module.exports = UserTempModal;
