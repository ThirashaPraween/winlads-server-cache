const mongoose = require("mongoose");

const OneoffSchema = new mongoose.Schema({
    uid: String,
    roundid: String,
    type: String,
    createdat: { type: Date, default: Date.now() },
});

const OneoffRafflesModel = mongoose.model("oneoffraffles", OneoffSchema);
module.exports = OneoffRafflesModel;