const mongoose = require("mongoose");

const InstaFormRequestsSchema = new mongoose.Schema({
  uid: String,
  instausername: String,
  instalink: String,
  createdat: { type: Date, default: Date.now() },
});

const InstaFormRequestsModel = mongoose.model("instaformrequests", InstaFormRequestsSchema);
module.exports = InstaFormRequestsModel;
