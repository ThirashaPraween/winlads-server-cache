const mongoose = require("mongoose");

const CouponsSchema = new mongoose.Schema({
  name: String,
  coupen: String,
  description: String,
  roundid: String,
  chance: { type: Number, default: 0 },
  count: { type: Number, default: 1 },
  status: { type: Number, default: 1 },
  createdat: { type: Date, default: Date.now() },
});

const CouponsModel = mongoose.model("coupons", CouponsSchema);
module.exports = CouponsModel;
