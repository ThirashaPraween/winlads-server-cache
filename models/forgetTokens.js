const mongoose = require("mongoose");

const ForgetTokenSchema = new mongoose.Schema({
  uid: String,
  email: String,
  token: String,
  status:{ type: Number, default: 1 },
  createdat: { type: Date, default: Date.now() },
});

const ForgetTokenModel = mongoose.model("forgetTokens", ForgetTokenSchema);
module.exports = ForgetTokenModel;
