const mongoose = require("mongoose");

const SubSchema = new mongoose.Schema({
    uid: String,
    subid: String,
    subscriptionid: String,
    status: Number,
    type: String,
    tial: Number,
    createdat: { type: Date, default: Date.now() },
});

const SubsciptionModel = mongoose.model("subscriptions", SubSchema);
module.exports = SubsciptionModel;
