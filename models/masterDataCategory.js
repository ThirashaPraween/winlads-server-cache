const mongoose = require("mongoose");

const masterDataCategorySchema = new mongoose.Schema({
    name: String,
    description: String,
    status:  { type: Number, default: 1 },
    createdat: { type: Date, default: Date.now() },
});

const MasterDataCategoryModel = mongoose.model("masterDataCategory", masterDataCategorySchema);
module.exports = MasterDataCategoryModel;
