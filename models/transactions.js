const mongoose = require("mongoose");

const TransactionSchema = new mongoose.Schema({
    uid:String,
    transactiontype:String,
    type:String,
    subid:String,
    roundid:String,
    sessionid: String,
    amount: String,
    currency:String,
    customer:String,
    customeremail:String,
    mode:String,
    paymentintent:String,
    subscriptionid:String,
    startfrom:Date,
    endfrom:Date,
    paymentmethod:String,
    invoice:String,
    createdat: { type: Date, default: Date.now() },
});

const TransactionModel = mongoose.model("transactions", TransactionSchema);
module.exports = TransactionModel;