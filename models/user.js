const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  email: String,
  mobile: String,
  nic: String,
  licencse: String,
  tin: Number,
  dob: Date,
  address: String,
  address2: String,
  city: String,
  state: String,
  postalcode: String,
  refferalId: String,
  uid: String,
  stripe_id: String,
  sub_id: String,
  subscriptionid:String,
  trial: { type: Boolean, default: false },
  trialend: Date,
  fcmtoken:String,
  image: String,
  password:String,
  chance: { type: Number, default: 0 },
  balance: { type: Number, default: 0.00 },
  createdat: { type: Date, default: Date.now() },
});

const UserModal = mongoose.model("user", UserSchema);
module.exports = UserModal;
