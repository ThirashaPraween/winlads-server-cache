const mongoose = require("mongoose");

const UserCouponsSchema = new mongoose.Schema({
  uid: String,
  coupenid: String,
  createdat: { type: Date, default: Date.now() },
});

const UserCouponsModel = mongoose.model("usercoupons", UserCouponsSchema);
module.exports = UserCouponsModel;
