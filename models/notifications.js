const mongoose = require("mongoose");

const NotificationsSchema = new mongoose.Schema({
  image: String,
  title: String,
  subtitle: String,
  desc: String,
  channel: String,
  createdat: { type: Date, default: Date.now() },
});

const NotificationsModel = mongoose.model("notifications", NotificationsSchema);
module.exports = NotificationsModel;
 