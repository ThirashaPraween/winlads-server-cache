const mongoose = require("mongoose");

const BusinessCardSchema = new mongoose.Schema({
  name: String,
  mobile: Number,
  passport: Number,
  address: String,
  address2: String,
  city: String,
  state: String,
  postalcode: String,
  uid: String,
  qr: String,
  requested: String,
  createdat: { type: Date, default: Date.now() },
});

const BusinessCardModel = mongoose.model("businesscard", BusinessCardSchema);
module.exports = BusinessCardModel;
