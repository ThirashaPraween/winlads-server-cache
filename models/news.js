const mongoose = require("mongoose");

const NewsSchema = new mongoose.Schema({
  image: String,
  maintitle: String,
  mainsubtitle: String,
  newstitle: String,
  desc: String,
  createdat: { type: Date, default: Date.now() },
});

const NewsModel = mongoose.model("news", NewsSchema);
module.exports = NewsModel;
