const TransactionModel = require("../models/transactions");
const UserModal = require("../models/user");
const nodemailer = require('nodemailer');
const fs = require('fs');
const pdf = require('pdf-creator-node');
const QRCode = require('qrcode');
const { getStorage } = require("firebase-admin/storage");
const stream = require('stream');
const SubsciptionPlansModel = require("../models/subscriptionplans");
const UserCouponsModel = require("../models/usercoupon");

module.exports = {
  createTransaction: async function (trans) {
    const user = await UserModal.findOne({ uid: trans.uid });
    let pay = true;
    if (trans.type == "balance") {
      if (trans.amount <= user.balance) {
        pay = true;
      } else {
        pay = false;
      }
    } else {
      pay = true;
    }

    if (pay) {
      try {
        let plan = await SubsciptionPlansModel.findOne({ subid: trans.subid });
        if (!plan) {
          plan = await SubsciptionPlansModel.findOne({ subidannual: trans.subid });
          if (!plan) {
            plan = await SubsciptionPlansModel.findOne({ subidsemiannual: trans.subid });
          }
        }

        const transaction = await TransactionModel.create(trans);
        if (trans.type == "balance") {
          user.balance = parseFloat(user.balance) - transaction.amount;
          user.save();
        }

        if (trans.mode == "subscription" && trans.type != "balance" && plan.name != "Starter") {
          const paybleAmount = transaction.amount;

          let fmAmount = 0;
          let smAmount = 0;
          let tmAmount = 0;
          let ffmAmount = 0;

          if (user.refferalId != '') {
            const fauser = await UserModal.findOne({ uid: user.refferalId });
            if (fauser) {
              let faplan = await SubsciptionPlansModel.findOne({ subid: fauser.sub_id });
              if (!faplan) {
                faplan = await SubsciptionPlansModel.findOne({ subidannual: fauser.sub_id });
                if (!faplan) {
                  faplan = await SubsciptionPlansModel.findOne({ subidsemiannual: fauser.sub_id });
                }
              }

              if (faplan.name != "Starter") {
                fmAmount = (paybleAmount / 100) * parseFloat(faplan.refferal_commision_l1);

                await TransactionModel.create({
                  uid: fauser.uid,
                  transactiontype: "CR",
                  type: "balance",
                  subid: "",
                  sessionid: "",
                  amount: fmAmount,
                  currency: "",
                  customer: "",
                  customeremail: "",
                  mode: "earning",
                  paymentintent: "",
                  subscriptionid: "",
                  startfrom: Date.now(),
                  endfrom: Date.now(),
                  paymentmethod: "",
                  invoice: "",
                });

                fauser.balance = parseFloat(fauser.balance) + fmAmount;
                fauser.save();
              }

              if (fauser.refferalId != '') {
                const sauser = await UserModal.findOne({ uid: fauser.refferalId });
                if (sauser) {
                  let saplan = await SubsciptionPlansModel.findOne({ subid: sauser.sub_id });
                  if (!saplan) {
                    saplan = await SubsciptionPlansModel.findOne({ subidannual: sauser.sub_id });
                    if (!saplan) {
                      saplan = await SubsciptionPlansModel.findOne({ subidsemiannual: sauser.sub_id });
                    }
                  }

                  if (saplan.name != "Starter") {
                    smAmount = (fmAmount / 100) * parseFloat(saplan.refferal_commision_l2);

                    await TransactionModel.create({
                      uid: sauser.uid,
                      transactiontype: "CR",
                      type: "balance",
                      subid: "",
                      sessionid: "",
                      amount: smAmount,
                      currency: "",
                      customer: "",
                      customeremail: "",
                      mode: "earning",
                      paymentintent: "",
                      subscriptionid: "",
                      startfrom: Date.now(),
                      endfrom: Date.now(),
                      paymentmethod: "",
                      invoice: "",
                    });

                    sauser.balance = parseFloat(sauser.balance) + smAmount;
                    sauser.save();
                  }

                  if (sauser.refferalId != '') {
                    const tauser = await UserModal.findOne({ uid: sauser.refferalId });
                    if (tauser) {
                      let taplan = await SubsciptionPlansModel.findOne({ subid: tauser.sub_id });
                      if (!taplan) {
                        taplan = await SubsciptionPlansModel.findOne({ subidannual: tauser.sub_id });
                        if (!taplan) {
                          taplan = await SubsciptionPlansModel.findOne({ subidsemiannual: tauser.sub_id });
                        }
                      }

                      if (taplan.name != "Starter") {
                        tmAmount = (smAmount / 100) * parseFloat(taplan.refferal_commision_l3);

                        await TransactionModel.create({
                          uid: tauser.uid,
                          transactiontype: "CR",
                          type: "balance",
                          subid: "",
                          sessionid: "",
                          amount: tmAmount,
                          currency: "",
                          customer: "",
                          customeremail: "",
                          mode: "earning",
                          paymentintent: "",
                          subscriptionid: "",
                          startfrom: Date.now(),
                          endfrom: Date.now(),
                          paymentmethod: "",
                          invoice: "",
                        });
                      }

                      tauser.balance = parseFloat(tauser.balance) + tmAmount;
                      tauser.save();

                      if (tauser.refferalId != '') {
                        const ffauser = await UserModal.findOne({ uid: tauser.refferalId });
                        if (ffauser) {
                          let ffaplan = await SubsciptionPlansModel.findOne({ subid: ffauser.sub_id });
                          if (!ffaplan) {
                            ffaplan = await SubsciptionPlansModel.findOne({ subidannual: ffauser.sub_id });
                            if (!ffaplan) {
                              ffaplan = await SubsciptionPlansModel.findOne({ subidsemiannual: ffauser.sub_id });
                            }
                          }

                          if (ffaplan.name != "Starter") {
                            ffmAmount = (tmAmount / 100) * parseFloat(ffaplan.refferal_commision_l4);

                            await TransactionModel.create({
                              uid: ffauser.uid,
                              transactiontype: "CR",
                              type: "balance",
                              subid: "",
                              sessionid: "",
                              amount: ffmAmount,
                              currency: "",
                              customer: "",
                              customeremail: "",
                              mode: "earning",
                              paymentintent: "",
                              subscriptionid: "",
                              startfrom: Date.now(),
                              endfrom: Date.now(),
                              paymentmethod: "",
                              invoice: "",
                            });
                          }

                          ffauser.balance = parseFloat(ffauser.balance) + ffmAmount;
                          ffauser.save();
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }

        return {
          status: 200,
          data: { message: 'Successfull' }
        };
      } catch (error) {
        return {
          status: 401,
          data: { message: 'Unknown error' }
        };
      }
    } else {
      return {
        status: 400,
        data: { message: 'inssuficient balance' }
      };
    }
  },
  sendEmail: async function (to, subject, body, file = null, path = null) {
    try {
      var transporter = nodemailer.createTransport({
        host: process.env.EMAIL_SERVER,
        port: process.env.EMAIL_PORT,
        auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_PASSWORD,
        }
      });

      message = {
        from: process.env.EMAIL_FROM,
        to: to,
        subject: subject,
        html: body,
        attachments: file != null ? [
          {
            filename: file,
            content: path,
          }
        ] : []
      }

      transporter.sendMail(message, function (err, info) {
        if (err) {
          console.log(err);
          return false;
        } else {
          return true;
        }
      });
    } catch (err) {
      return false;
    }
  },
  createPdf: async function (uid) {
    const user = await UserModal.findOne({ uid: uid });
    const text = `uid - ${user?.uid}\nname - ${user?.name}\nmoble - ${user?.mobile}\naddress - ${user?.address}\n`;
    try {
      var bucket = getStorage().bucket();
      const file = bucket.file(uid + '.png');

      file.exists()
        .then(async (exists) => {
          if (exists[0]) {

          } else {
            const qr = await QRCode.toDataURL(text);
            let bufferStream = new stream.PassThrough();
            bufferStream.end(new Buffer.from(qr.split(',')[1], 'base64'));

            bufferStream.pipe(file.createWriteStream({
              metadata: {
                contentType: 'image/png'
              }
            }))
              .on('error', error => {

              })
              .on('finish', (file) => {

              });
          }
        })

      var html = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Document</title></head><body style="margin: 0px; padding: 0;"><!-- REMOVE WIDTH AND MARGIN IF NEED FULL WIDTH --><div style="width:600px;box-sizing: border-box; margin: 20px auto; background-color: "white"; display: flex; align-items: center; justify-content: center; flex-direction: column;"><div><h1>Winlads Personalized NFC Card</h1></div><div style="padding: 20px; background-color: black; width: 100%;border-radius: 20px; overflow: hidden;margin-bottom: 20px;"><div style="position: relative;"><div style="width: 200px;"><img src="{{this.server}}/public/images/logo.png" style="width: 100%; height: 100%; object-fit: contain;"></div><div style="width: 30px; position: absolute; top: 0px;right: 0px;"><img src="{{this.server}}/public/images/Signal.png" alt="NFC Icon" style="width: 100%; height: 100%; object-fit: contain;"></div></div><div style="width: 100%;"><img src="{{this.server}}/public/images/jip.png" style="width: 100%; height: 100%; object-fit: contain;"></div><div style="text-align: center;"><h5 style="color: #3fb0c2; font-weight: 500;">Connecting Hearts,uplifting lives :our people centric giveaways</h5></div></div><div style="padding: 20px; background-color: black; width: 100%;border-radius: 20px; overflow: hidden;"><div style="display: flex; align-items: center; justify-content: space-between;width: 100%;"><div style="width: 80px;"><img src="{{this.server}}/public/images/icon.png" style="width: 100%; height: 100%; object-fit: contain;"></div></div><div style="text-align: center;"><!-- QR CODE AREA WILL BE FIXED SIZE MINIMUM 150PX AND MAX 200PX --><div style="padding: 10px;min-width: 150px; aspect-ratio: 1/1; border: 2px solid white; max-width: 200px; margin: 0px auto;border-radius: 20px;"><img src="{{this.qrlink}}" style="width: 100%; height: 100%; object-fit: contain;"></div><h5 style="color: #3fb0c2; font-weight: 500;">giveaways@winlads.com.au</h5></div></div></div></body></html>';

      const qrlink = await file.getSignedUrl({
        action: 'read',
        expires: '03-09-2491'
      });

      var document = {
        html: html,
        data: {
          server: process.env.SELF_ADDRESS,
          qrlink: qrlink
        },
        type: "buffer",
      };

      var options = {
        format: "A4",
        orientation: "portrait",
        border: "10mm",
      };

      const doc = await pdf.create(document, options);
      const pdffile = bucket.file(uid + '.pdf');

      let bufferStream = new stream.PassThrough();
      bufferStream.end(new Buffer.from(doc))
      bufferStream.pipe(pdffile.createWriteStream({
        metadata: {
          contentType: 'application/pdf'
        }
      })).on('error', error => {
        return 'here';
      })
        .on('finish', (file) => {

        });

      const pdflink = await pdffile.getSignedUrl({
        action: 'read',
        expires: '03-09-2491'
      });

      if (doc) {
        return pdflink;
      } else {
        return null;
      }
    } catch (err) {
      console.log(err);
      return false;
    }
  },
  createPdfBuffer: async function (uid) {
    const user = await UserModal.findOne({ uid: uid });
    const text = `uid - ${user?.uid}\nname - ${user?.name}\nmoble - ${user?.mobile}\naddress - ${user?.address}\n`;
    try {
      var bucket = getStorage().bucket();
      const file = bucket.file(uid + '.png');

      file.exists()
        .then(async (exists) => {
          if (exists[0]) {

          } else {
            const qr = await QRCode.toDataURL(text);
            let bufferStream = new stream.PassThrough();
            bufferStream.end(new Buffer.from(qr.split(',')[1], 'base64'));

            bufferStream.pipe(file.createWriteStream({
              metadata: {
                contentType: 'image/png'
              }
            }))
              .on('error', error => {

              })
              .on('finish', (file) => {

              });
          }
        })

      var html = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Document</title></head><body style="margin: 0px; padding: 0;"><!-- REMOVE WIDTH AND MARGIN IF NEED FULL WIDTH --><div style="width:600px;box-sizing: border-box; margin: 20px auto; background-color: "white"; display: flex; align-items: center; justify-content: center; flex-direction: column;"><div><h1>Winlads Personalized NFC Card</h1></div><div style="padding: 20px; background-color: black; width: 100%;border-radius: 20px; overflow: hidden;margin-bottom: 20px;"><div style="position: relative;"><div style="width: 200px;"><img src="{{this.server}}/public/images/logo.png" style="width: 100%; height: 100%; object-fit: contain;"></div><div style="width: 30px; position: absolute; top: 0px;right: 0px;"><img src="{{this.server}}/public/images/Signal.png" alt="NFC Icon" style="width: 100%; height: 100%; object-fit: contain;"></div></div><div style="width: 100%;"><img src="{{this.server}}/public/images/jip.png" style="width: 100%; height: 100%; object-fit: contain;"></div><div style="text-align: center;"><h5 style="color: #3fb0c2; font-weight: 500;">Connecting Hearts,uplifting lives :our people centric giveaways</h5></div></div><div style="padding: 20px; background-color: black; width: 100%;border-radius: 20px; overflow: hidden;"><div style="display: flex; align-items: center; justify-content: space-between;width: 100%;"><div style="width: 80px;"><img src="{{this.server}}/public/images/icon.png" style="width: 100%; height: 100%; object-fit: contain;"></div></div><div style="text-align: center;"><!-- QR CODE AREA WILL BE FIXED SIZE MINIMUM 150PX AND MAX 200PX --><div style="padding: 10px;min-width: 150px; aspect-ratio: 1/1; border: 2px solid white; max-width: 200px; margin: 0px auto;border-radius: 20px;"><img src="{{this.qrlink}}" style="width: 100%; height: 100%; object-fit: contain;"></div><h5 style="color: #3fb0c2; font-weight: 500;">giveaways@winlads.com.au</h5></div></div></div></body></html>';

      const qrlink = await file.getSignedUrl({
        action: 'read',
        expires: '03-09-2491'
      });

      var document = {
        html: html,
        data: {
          server: process.env.SELF_ADDRESS,
          qrlink: qrlink
        },
        type: "buffer",
      };

      var options = {
        format: "A3",
        orientation: "portrait",
        border: "10mm",
      };

      const doc = await pdf.create(document, options);
      if (doc) {
        return doc;
      } else {
        return null;
      }
    } catch (err) {
      console.log(err);
      return false;
    }
  },
  getRefferalLimit: async function (uid) {
    const refuser = await UserModal.findOne({ uid: uid });
    if (refuser) {
      let limit = 0;
      let plan = await SubsciptionPlansModel.findOne({ subid: refuser.sub_id });
      // console.log(plan, refuser.sub_id)
      if (plan) {
        limit = plan?.refferal_limit;
      } else {
        plan = await SubsciptionPlansModel.findOne({ subidannual: refuser.sub_id });
        if (plan) {
          limit = plan?.refferal_limit;
        } else {
          plan = await SubsciptionPlansModel.findOne({ subidsemiannual: refuser.sub_id });
          limit = plan?.refferal_limit;
        }
      }

      const count = await UserModal.count({ refferalId: uid });
      // console.log(count, uid, limit)
      if (count < limit) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  },
  getSubscription: async function (subid) {
    let subscription = await SubsciptionPlansModel.findOne({ subid: subid });
    if (!subscription) {
      subscription = await SubsciptionPlansModel.findOne({ subidannual: subid });
      if (!subscription) {
        subscription = await SubsciptionPlansModel.findOne({ subidsemiannual: subid });
        if (subscription) {
          return {
            type: 'semiannual',
            subid: subid,
            priceid: subscription.price_id_semiannual,
            count: subscription.raffle_count_semiannual,
            price: subscription.semiannualy,
            data:subscription
          }
        }
      } else {
        return {
          type: 'annual',
          subid: subid,
          priceid: subscription.price_id_annual,
          count: subscription.raffle_count_annual,
          price: subscription.annualy,
          data:subscription
        }
      }
    } else {
      return {
        type: 'monthly',
        subid: subid,
        priceid: subscription.price_id,
        count: subscription.raffle_count,
        price: subscription.monthly,
        data:subscription
      }
    }
  },
  getSubscriptionbyprice: async function (priceid) {
    let subscription = await SubsciptionPlansModel.findOne({ price_id: priceid });
    if (!subscription) {
      subscription = await SubsciptionPlansModel.findOne({ price_id_annual: priceid });
      if (!subscription) {
        subscription = await SubsciptionPlansModel.findOne({ price_id_semiannual: priceid });
        if (subscription) {
          return {
            type: 'semiannual',
            subid: subscription.subidsemiannual,
            priceid: subscription.price_id_semiannual,
            count: subscription.raffle_count_semiannual,
            price: subscription.semiannualy
          }
        }
      } else {
        return {
          type: 'annual',
          subid: subscription.subidannual,
          priceid: subscription.price_id_annual,
          count: subscription.raffle_count_annual,
          price: subscription.annualy
        }
      }
    } else {
      return {
        type: 'monthly',
        subid: subscription.subid,
        priceid: subscription.price_id,
        count: subscription.raffle_count,
        price: subscription.monthly
      }
    }
  },
  asyncForeach: function (callback) {
    return Promise.resolve(this).then(async (ar) => {
      for (let i = 0; i < ar.length; i++) {
        await callback.call(ar, ar[i], i, ar);
      }
    });
  },
  checkCoupon: async function (uid, coupenid) {
    const coupons = await UserCouponsModel.findOne({ uid: uid, coupenid: coupenid });
    if (coupons) {
      return false;
    } else {
      return true;
    }
  },
  expireCoupon: async function (uid, couponid) {
    const coupon = await UserCouponsModel.create({ uid: uid, coupenid: couponid });
    if (coupon) {
      return true;
    } else {
      return false;
    }
  }
}