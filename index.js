const express = require("express");
require('dotenv').config();
const mongoose = require('mongoose');
const cors = require("cors");
const UserModal = require("./models/user");
const RafflesModal = require("./models/raffels");
const RaffleRoundsModal = require("./models/raffleRounds");
const Stripe = require("stripe");
const SubsciptionModel = require("./models/subscriptions");
const SubsciptionPlansModel = require("./models/subscriptionplans");
const TransactionModel = require("./models/transactions");
const moment = require('moment');
const UserRafflesModel = require("./models/userRaffles");
const FaqModel = require("./models/faq");
const NewsModel = require("./models/news");
const QRCode = require('qrcode');
const fs = require('fs');
const path = require('path');
const BusinessCardModel = require("./models/businessCard");
const BalanceTransferModel = require("./models/balanceTransferRequests");
const NotificationsModel = require("./models/notifications");
var firebase = require("firebase-admin");
var serviceAccount = require("./winland-58673-59fc56f22cb1.json");
const functions = require('./common/functions');
const { getStorage } = require("firebase-admin/storage");
const stream = require('stream');
const MasterDataCategoryModel = require("./models/masterDataCategory");
const MasterDataDetailsModel = require("./models/masterDataDetails");
const AdminModal = require("./models/adminModel");
const { createToken, LoginValidator } = require("./middlewares/UserValidation");
const { forEachAsync } = require("foreachasync");
const bcrypt = require("bcrypt");
const ForgetTokenModel = require("./models/forgetTokens");
const CouponsModel = require("./models/coupons");
const InstaFormRequestsModel = require("./models/instaFormRequests");
const UserTempModal = require("./models/userTemp");
const UserCouponsModel = require("./models/usercoupon");
const OneoffRafflesModel = require("./models/oneoffRaffles");

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    storageBucket: "gs://winland-58673.appspot.com",
});

module.exports = { firebase }

const app = express()
const PORT = process.env.PORT

app.use(express.json())
app.use(cors())
app.use('/public', express.static(path.join(__dirname, 'public')))
mongoose.connect(process.env.MONGODB_DRIVER_URL)

const stripe = new Stripe(process.env.STRIPE_API_KEY);

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * Date.now()) | 0).toString(35).substring(1);
    };
    return (S4() + S4() + S4());
}

app.get('/usage', (req, res) => {
    res.json({
        "status": 200,
        "data": "Online",
    })
});

app.post('/register', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    let user;
    const saltRounds = 10;
    let password = '';
    if (req.body.password) {
        try {
            req.body.password = await bcrypt.hash(req.body.password, saltRounds);
        } catch (error) {
            //console.log(error);
        }
    }
    // console.log(req.body.refferalId)
    if (req.body.refferalId) {
        const limitNotExceeded = await functions.getRefferalLimit(req.body.refferalId);
        if (!limitNotExceeded) {
            delete req.body['refferalId'];
        }
    }

    const existingUser = await UserModal.findOne({ email: req.body.email });
    if (!existingUser) {
        user = await UserModal.create(req.body);
        if (user) {
            try {
                await functions.sendEmail(user.email, 'Registration Confirmation for Winlads Giveaway Program', `<div><img src="https://winlads-api.com/img/win1000ban.png"></img><p>Dear ${user.firstname},</p><p>We are thrilled to inform you that your registration for the Winlads Giveaway Program has been successfully completed. We are excited to have you on board and look forward to your participation in our upcoming giveaways.
                 <br>
                 <br>
                 As a registered user, you will have the opportunity to participate in various exciting contests and win amazing prizes. Keep an eye on your email for updates on upcoming giveaways and exclusive offers. 
                 <br>
                 <br>
                 Thank you for joining Winlads and being a part of our community. If you have any questions or need further assistance, feel free to reach out to our support team at support@winlads.com
                 <br>
                 <br>
                 Once again, welcome to Winlads and best of luck in our upcoming giveaways! 
                 <br>
                 <br>
                 Sincerely, 
                <br>
                <img src="https://winlads-api.com/img/50win.png"></img><br>
                Winlads Team
  </p><div>`);
                if (req.body.coupen) {
                    const coupen = await CouponsModel.findOne({ coupen: req.body.coupen });

                    if (coupen) {
                        const coupenVaild = await functions.checkCoupon(req.body.uid, coupen._id);

                        if (coupenVaild) {
                            const counts = Array.from(
                                { length: (coupen?.count - 1) / 1 + 1 }, (value, index) => 1 + index * 1
                            );
                            const Round = await RaffleRoundsModal.findOne({ _id: coupen?.roundid });
                            const entryNo = Round.lastno;

                            forEachAsync(counts, async function (count) {

                                await UserRafflesModel.create({
                                    uid: req.body.uid,
                                    raffleroundid: coupen.roundid,
                                    status: 1,
                                    subid: "",
                                    type: "coupon",
                                    createddate: Date.now(),
                                    entryNumber: entryNo + count
                                });
                            }).then(async function () {
                                await functions.expireCoupon(req.body.uid, coupen._id);
                                Round.lastno = entryNo + counts.length;
                                Round.save();
                            });
                        }
                    }
                }
            } catch (error) {

            }
            res.json({
                "status": 200,
                "data": user,
                message: 'User registered successfully',
            })
        } else {
            res.json({
                "status": 400,
                "data": err,
                message: 'Failed to register',
            })
        }
    } else {
        res.json({
            "status": 400,
            "data": "User already registered",
            message: 'User already registered',
        })
    }
});

app.post('/registerWithStripe', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const saltRounds = 10;
        if (req.body.password) {
            try {
                req.body.password = await bcrypt.hash(req.body.password, saltRounds);
            } catch (error) {
                //console.log(error);
            }
        }

        if (req.body.refferalId) {
            const limitNotExceeded = await functions.getRefferalLimit(req.body.refferalId);
            if (!limitNotExceeded) {
                delete req.body['refferalId'];
            }
        }

        const existingUser = await UserModal.findOne({ email: req.body.email });
        if (!existingUser) {
            const tempuser = await UserTempModal.create(req.body);
            if (tempuser) {
                if (req.body.type == 'subscription') {
                    const subscription = await functions.getSubscription(req.body.subid);
                    if (subscription) {
                        try {
                            const session = await stripe.checkout.sessions.create({
                                success_url: `${process.env.SELF_ADDRESS}/saveUser?type=1&sub_id=${req.body.subid}&temp_id=${tempuser._id}&session_id={CHECKOUT_SESSION_ID}&coupen=${req.body.coupen || ''}`,
                                line_items: [
                                    { price: subscription.priceid, quantity: 1 },
                                ],
                                mode: 'subscription',
                                allow_promotion_codes: true
                            });

                            res.json({
                                status: 200,
                                message: 'Paylink accepted',
                                payurl: session?.url
                            });
                        } catch (error) {
                            res.json({
                                status: 401,
                                message: 'Error creating paylink',
                            });
                        }
                    } else {
                        res.json({
                            status: 401,
                            message: 'Subscription plan not found',
                        });
                    }
                }else if (req.body.type == 'trial') {
                    const subscription = await functions.getSubscription(req.body.subid);
                    if (subscription) {
                        try {
                            const session = await stripe.checkout.sessions.create({
                                success_url: `${process.env.SELF_ADDRESS}/saveUser?type=3&sub_id=${req.body.subid}&temp_id=${tempuser._id}&session_id={CHECKOUT_SESSION_ID}&coupen=${req.body.coupen || ''}`,
                                line_items: [
                                    { price: subscription.priceid, quantity: 1 },
                                ],
                                mode: 'subscription',
                                allow_promotion_codes: true,
                                subscription_data: {
                                    trial_settings: {
                                        end_behavior: {
                                            missing_payment_method: 'cancel',
                                        },
                                    },
                                    trial_period_days: 30,
                                },
                            });
                            res.json({
                                status: 200,
                                message: 'paylink accepted',
                                payurl: session?.url
                            });
                        } catch (error) {
                            res.json({
                                status: 401,
                                message: 'Error creating paylink',
                            });
                        }
                    } else {
                        res.json({
                            status: 401,
                            message: 'Subscription plan not found',
                        });
                    }
                } else {
                    const Round = await RaffleRoundsModal.findOne({ _id: req.body.roundid });
                    if (Round) {
                        let price = (Round.price * 100);
                        if (req.body.coupen && req.body.coupen === 'WIN50OFF') {
                            price = price / 2;
                        }

                        try {
                            const session = await stripe.checkout.sessions.create({
                                success_url: `${process.env.SELF_ADDRESS}/saveUser?type=2&roundid=${req.body.roundid}&count=${req.body.count}&temp_id=${tempuser._id}&session_id={CHECKOUT_SESSION_ID}&coupen=${req.body.coupen || ''}`,
                                line_items: [
                                    {
                                        price_data: {
                                            currency: "aud",
                                            unit_amount: price,
                                            product_data: {
                                                name: Round.name,
                                            },
                                        },
                                        quantity: 1
                                    },
                                ],
                                mode: 'payment',
                                allow_promotion_codes: true
                            });
                            res.json({
                                status: 200,
                                message: 'paylink accepted',
                                payurl: session?.url
                            });
                        } catch (error) {
                            res.json({
                                status: 401,
                                message: 'Error creating paylink',
                            });
                        }
                    } else {
                        res.json({
                            status: 401,
                            message: 'Giveaway not found',
                        });
                    }
                }
            } else {
                res.json({
                    "status": 400,
                    message: 'Failed to register',
                });
            }
        } else {
            res.json({
                "status": 400,
                message: 'User already exists',
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get("/saveUser", async (req, res) => {
    const session = await stripe.checkout.sessions.retrieve(
        req.query.session_id, {
        expand: ['subscription'],
    }
    );

    if (session) {
        if (session.payment_status == "paid") {
            const tempuser = await UserTempModal.findOne({ _id: req.query.temp_id });
            const user = await UserModal.create({
                firstname: tempuser.firstname,
                lastname: tempuser.lastname,
                email: tempuser.email,
                mobile: tempuser.mobile,
                nic: tempuser.nic,
                licencse: tempuser.licencse,
                tin: tempuser.tin,
                dob: tempuser.dob,
                address: tempuser.address,
                address2: tempuser.address2,
                city: tempuser.city,
                state: tempuser.state,
                postalcode: tempuser.postalcode,
                refferalId: tempuser.refferalId,
                uid: tempuser.uid,
                image: tempuser.image,
                password: tempuser.password,
            });

            if (user) {

                if (req.query.coupen != '') {
                    const coupen = await CouponsModel.findOne({ coupen: req.query.coupen });
                    if (coupen) {
                        const coupenVaild = await functions.checkCoupon(user.uid, coupen._id);
                        if (coupenVaild) {
                            if(coupen.chance==0){
                                const counts = Array.from(
                                    { length: (coupen?.count - 1) / 1 + 1 }, (value, index) => 1 + index * 1
                                );
                                const Round = await RaffleRoundsModal.findOne({ _id: coupen?.roundid });
                                const entryNo = Round.lastno;
    
                                forEachAsync(counts, async function (count) {
                                    await UserRafflesModel.create({
                                        uid: user.uid,
                                        raffleroundid: coupen.roundid,
                                        status: 1,
                                        subid: "",
                                        type: "coupon",
                                        createddate: Date.now(),
                                        entryNumber: entryNo + count
                                    });
                                }).then(async function () {
                                    await functions.expireCoupon(user.uid, coupen._id);
                                    Round.lastno = entryNo + counts.length;
                                    Round.save();
                                });
                            }else{
                                await functions.expireCoupon(user.uid, coupen._id);
                                user.chance=1;
                                user.save();
                            }
                        }
                    }
                }

                try {
                    await functions.sendEmail(user.email, 'Registration Confirmation for Winlads Giveaway Program', `<div><img src="https://winlads-api.com/img/win1000ban.png"></img><p>Dear ${user.firstname},</p><p>We are thrilled to inform you that your registration for the Winlads Giveaway Program has been successfully completed. We are excited to have you on board and look forward to your participation in our upcoming giveaways.
                 <br>
                 <br>
                 As a registered user, you will have the opportunity to participate in various exciting contests and win amazing prizes. Keep an eye on your email for updates on upcoming giveaways and exclusive offers. 
                 <br>
                 <br>
                 Thank you for joining Winlads and being a part of our community. If you have any questions or need further assistance, feel free to reach out to our support team at support@winlads.com
                 <br>
                 <br>
                 Once again, welcome to Winlads and best of luck in our upcoming giveaways! 
                 <br>
                 <br>
                 Sincerely, 
                <br>
                <img src="https://winlads-api.com/img/50win.png"></img><br>
                Winlads Team
  </p><div>`);
                } catch (error) {

                }

                if (req.query.type == 1) {
                    const paymentIntentId = session.payment_intent ? session.payment_intent.id : null;

                    await functions.createTransaction({
                        uid: user.uid,
                        transactiontype: "DR",
                        type: "stripe",
                        subid: req.query.sub_id,
                        sessionid: session.id,
                        amount: session.amount_total / 100,
                        currency: session.currency,
                        customer: session.customer,
                        customeremail: session.customer_details.email,
                        mode: session.mode,
                        paymentintent: paymentIntentId,
                        subscriptionid: session.subscription.id,
                        startfrom: moment.unix(session.subscription.current_period_start),
                        endfrom: moment.unix(session.subscription.current_period_end),
                        paymentmethod: session.subscription.default_payment_method,
                        invoice: session.subscription.latest_invoice,
                    });

                    const subscription = await functions.getSubscription(req.query.sub_id);

                    const rafflesRounds = await RaffleRoundsModal.find({
                        startingtime: {
                            $gt: Date.now()
                        }
                    });

                    forEachAsync(rafflesRounds, async function (e) {
                        let cr = e.lastno;

                        for (var i = 0; i < subscription.count; i++) {
                            await UserRafflesModel.create({
                                uid: user.uid,
                                raffleroundid: e._id,
                                subid: req.query.sub_id,
                                type: "subscription",
                                createddate: Date.now(),
                                entryNumber: cr,
                            });
                            cr += 1;
                        }

                        const round = await RaffleRoundsModal.findOne({ _id: e._id });
                        round.lastno = cr;
                        round.save();
                    }).then(async function () {
                        await SubsciptionModel.create({
                            uid: user.uid,
                            subid: req.query.sub_id,
                            subscriptionid: session.subscription.id,
                            type:"Stripe",
                            trail:0,
                            status: 1
                        });

                        UserModal.findOneAndUpdate({ uid: user.uid }, {
                            sub_id: req.query.sub_id,
                            subscriptionid: session.subscription.id,
                            stripe_id: session.customer
                        }, {
                            new: true
                        }).
                            then(user => {
                                res.redirect(process.env.CLIENT_ADDRESS.concat(`/subscription-done?suc=1&sub_id=${req.query.sub_id}&id=${user._id}`))
                            })
                            .catch(err => {
                                res.redirect(process.env.CLIENT_ADDRESS.concat("/subscription-done?suc=0&fail=1&id=${user._id}&sub_id="))
                            });
                    });
                } else if (req.query.type == 3) {
                    const subscription = await functions.getSubscription(req.query.sub_id);
                    const rafflesRounds = await RaffleRoundsModal.find({
                        startingtime: {
                            $gt: Date.now()
                        }
                    });

                    await SubsciptionModel.create({
                        uid: user.uid,
                        subid: req.query.sub_id,
                        subscriptionid: session.subscription.id,
                        type:"Stripe",
                        trial:1,
                        status: 1
                    });

                    UserModal.findOneAndUpdate({ uid: user.uid }, {
                        sub_id: req.query.sub_id,
                        subscriptionid: session.subscription.id,
                        stripe_id: session.customer,
                        trial: true,
                        trialend: moment().add(30, 'days')
                    }, {
                        new: true
                    }).
                        then(user => {
                            res.redirect(process.env.CLIENT_ADDRESS.concat(`/subscription-done?suc=1&sub_id=${req.query.sub_id}&id=${user._id}`))
                        })
                        .catch(err => {
                            res.redirect(process.env.CLIENT_ADDRESS.concat("/subscription-done?suc=0&fail=1&id=${user._id}&sub_id="))
                        });
                } else {
                    const paymentIntentId = session.payment_intent ? session.payment_intent.id : null;
                    await functions.createTransaction({
                        uid: user.uid,
                        transactiontype: "DR",
                        type: "stripe",
                        roundid: req.query.roundid,
                        sessionid: session.id,
                        amount: session.amount_total / 100,
                        currency: session.currency,
                        customer: session.customer,
                        customeremail: session.customer_details.email,
                        mode: session.mode,
                        paymentintent: paymentIntentId,
                        subscriptionid: "",
                        startfrom: Date.now(),
                        endfrom: Date.now(),
                        paymentmethod: "",
                        invoice: "",
                    });

                    const Round = await RaffleRoundsModal.findOne({ _id: req.query.roundid });
                    const count = req.query.count || 1;
                    const entryNo = Round.lastno;

                    const counts = Array.from(
                        { length: (count - 1) / 1 + 1 }, (value, index) => 1 + index * 1
                    );

                    forEachAsync(counts, async function (count) {

                        await UserRafflesModel.create({
                            uid: user.uid,
                            raffleroundid: req.query.roundid,
                            status: 1,
                            subid: "",
                            type: "cash",
                            createddate: Date.now(),
                            entryNumber: entryNo + count
                        });
                    }).then(async function () {
                        await OneoffRafflesModel.create({
                            uid: user.uid,
                            roundid:Round._id,
                            type:'REG'
                        });

                        Round.lastno = entryNo + counts.length;
                        Round.save();
                        res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-done?suc=1&round_id=${req.query.roundid}&id=${user._id}`))
                    });
                }
            } else {
                res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-failed?suc=0&msg=nouser`))
            }
        } else {
            res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-failed?suc=0&msg=not paid`))
        }
    } else {
        res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-failed?suc=0&msg=invalid active session`))
    }
});

app.post('/changePassword', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const saltRounds = 10;
        const user = await UserModal.findOne({ uid: req.body.uid });
        if (user) {
            bcrypt.compare(req.body.password, user.password, async function (err, result) {
                if (result) {
                    const newpass = await bcrypt.hash(req.body.newpassword, saltRounds);

                    user.password = newpass;
                    user.save();

                    const token = createToken(user._id);

                    res.json({
                        status: 200,
                        token: token,
                        data: user,
                        message: 'Password changed',
                    })
                } else {
                    res.json({
                        status: 401,
                        message: 'Old password is wrong',
                    });
                }
            });
        } else {
            res.json({
                status: 400,
                message: 'This email not registered',
            });
        }
    } catch (error) {
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getUserCount', async (req, res) => {
    try {
        const count = await UserModal.count();

        res.json({
            "status": 200,
            users: count
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getUsers', async (req, res) => {
    try {
        const users = await UserModal.find().sort({_id:-1});

        let usrs = [];

        forEachAsync(users, async function (user) {
            const sub = await functions.getSubscription(user.sub_id);
            user = { ...user._doc, sub };
            usrs.push(user);
        }).then(function () {
            res.json({
                "status": 200,
                users: usrs
            });
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getUser', async (req, res) => {
    try {
        let user = await UserModal.findOne({uid:req.query.uid});
        if(user){
            const sub = await functions.getSubscription(user.sub_id);
            const entries = await UserRafflesModel.find({uid:user.uid});
            const transactions = await TransactionModel.find({uid:user.uid});

            user = { ...user._doc, sub, entries, transactions };

            res.json({
                "status": 200,
                users: user
            });
        }else{
            res.json({
                "status": 401,
                message: "No user"
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/checkMobile', async (req, res) => {
    try {
        const mobile = req.query.mobile;
        const existingUser = await UserModal.findOne({ mobile: '+' + mobile.trim() });
        if (existingUser) {
            res.json({
                "status": 200,
                exists: true,
                message: 'Number already exists',
            });
        } else {
            res.json({
                "status": 400,
                exists: false,
                message: 'Number not exists',
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/checkEmail', async (req, res) => {
    try {
        const existingUser = await UserModal.findOne({ email: req.query.email });
        if (existingUser) {
            res.json({
                "status": 200,
                exists: true,
                message: 'Email already exists',
            });
        } else {
            res.json({
                "status": 400,
                exists: false,
                message: 'Email not exists',
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/validate', async (req, res) => {
    try {
        const id = req.query.id;

        if (id == "undefined" || id == undefined) {
            res.json({
                "status": 400,
                exists: false,

                data: null
            });
        } else {
            const existingUser = await UserModal.findOne({ _id: id });
            if (existingUser) {
                let subscripton = await SubsciptionPlansModel.findOne({ subid: existingUser.sub_id });

                if (!subscripton) {
                    subscripton = await SubsciptionPlansModel.findOne({ subidannual: existingUser.sub_id });
                }

                const transaction = await TransactionModel.findOne({ subid: existingUser.sub_id });

                let data = existingUser;
                data = { ...data._doc, subscripton, transaction };

                res.json({
                    "status": 200,
                    exists: true,
                    data: data
                });
            } else {
                res.json({
                    "status": 400,
                    exists: false,
                    data: null
                });
            }
        }

    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/login', async (req, res) => {
    try {
        const uid = req.query.uid;
        const existingUser = await UserModal.findOne({ uid: uid });
        const token = createToken(existingUser._id);
        if (existingUser) {
            res.json({
                "status": 200,
                exists: true,
                data: existingUser,
                token: token,
                message: 'User logged in successfully',
            });
        } else {
            res.json({
                "status": 400,
                exists: false,
                data: null,
                message: 'Failed to login',
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.post('/loginWithPassword', async (req, res) => {
    if (req.method != "POST") return res.status(400);

    try {
        const user = await UserModal.findOne({ email: req.body.email });
        if (user) {
            bcrypt.compare(req.body.password, user.password, function (err, result) {
                if (result) {
                    const token = createToken(user._id);
                    res.json({
                        status: 200,
                        token: token,
                        data: user
                    })
                } else {
                    res.json({
                        status: 400,
                        message: 'Failed to login',
                    });
                }
            });
        } else {
            res.json({
                status: 400,
                message: 'This email not registered',
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.post('/editUser', async (req, res) => {
    if (req.method != "POST") return res.status(400);

    try {
        await UserModal.findOneAndUpdate({ _id: req.body.id }, req.body, {
            new: true
        }).
            then(user => {
                res.json({
                    "status": 200,
                    "data": user
                })
            })
            .catch(err => {
                res.json({
                    "status": 400,
                    "data": err
                })
            });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.post('/subscriptionPlan', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const subid1 = new mongoose.Types.ObjectId();
        const subid2 = new mongoose.Types.ObjectId();
        const subid3 = new mongoose.Types.ObjectId();
        await SubsciptionPlansModel.create({
            name: req.body.name,
            desc: req.body.desc,
            color: req.body.color,
            subid: subid1,
            subidsemiannual: subid2,
            subidannual: subid3,
            monthly: req.body.monthly,
            semiannualy: req.body.semiannualy,
            annualy: req.body.annualy,
            price_id: req.body.priceid,
            price_id_semiannual: req.body.priceidsemiannual,
            price_id_annual: req.body.priceidannual,
            raffle_count: req.body.rafflecount,
            raffle_count_semiannual: req.body.rafflecountsemiannual,
            raffle_count_annual: req.body.rafflecountannual,
            refferal_limit: req.body.refferallimit,
            refferal_commision_l1: req.body.refferalcommisionl1,
            refferal_commision_l2: req.body.refferalcommisionl2,
            refferal_commision_l3: req.body.refferalcommisionl3,
            refferal_commision_l4: req.body.refferalcommisionl4,
            withdrawal_limit: req.body.withdrawlimit,
            withdrawal_timeframe: req.body.withdrawcount,
            withdrawal_timeframe_term: req.body.withdrawterm,
        }).
            then(Plan => {
                res.json({
                    "status": 200,
                    "data": Plan,
                })
            })
            .catch(err => {
                res.json({
                    "status": 400,
                    "data": err
                })
            });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getSubscriptionPlans', async (req, res) => {
    try {
        const subscriptionPlans = await SubsciptionPlansModel.find();
        if (subscriptionPlans) {
            res.json({
                "status": 200,
                data: subscriptionPlans
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

//enpoint for mobile application - get required keys to show payment sheet
app.post('/getStripeCustomer', async (req, res) => {
    if (req.method != "POST") return res.status(400);

    const { email, price } = req.body;

    try {
        //check this email exists on stripe
        let customer = await stripe.customers.search({
            query: 'email:\'' + email + '\'',
        });
        if (customer.data.length == 0) {
            //create a new customer using email
            customer = await stripe.customers.create({
                email,
            });
        } else {
            //get first customer
            customer = customer.data[0]
        }
        //should save this stripe customer id to database'
        const existingUser = await UserModal.findOne({ email: email });
        existingUser.stripe_id = customer.id
        existingUser.save();

        //get an ephemeral key using cus id
        const ephemeralKey = await stripe.ephemeralKeys.create(
            { customer: customer.id },
            { apiVersion: '2023-10-16' }
        );

        //create an incompleted subcsription for grab payment intent
        const subscription = await stripe.subscriptions.create({
            customer: customer.id,
            items: [
                { price: price },
            ],
            payment_behavior: 'default_incomplete',
            payment_settings: { save_default_payment_method: 'on_subscription' },
            expand: ['latest_invoice.payment_intent']
        });

        //sending out all details
        return res.json({
            paymentIntent: subscription.latest_invoice.payment_intent.client_secret,
            ephemeralKey: ephemeralKey.secret,
            customer: customer.id,
            subscription: subscription.id
        });

    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }

});

//enpoint for mobile application - save subscription data to database
app.post('/subscriptionMobile', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const { subscriptionId, uid, months, price } = req.body;
        const subscription = await stripe.subscriptions.retrieve(subscriptionId);

        if (subscription.status == 'active') {
            const sub = await functions.getSubscriptionbyprice(price);
            const existingUser = await UserModal.findOne({ uid: req.query.uid });

            try {
                await stripe.subscriptions.cancel(
                    existingUser.subscriptionid
                );
            } catch (error) {

            }

            await functions.createTransaction({
                uid: uid,
                transactiontype: "DR",
                type: "stripe",
                subid: sub.subid,
                sessionid: "",
                amount: subscription.items.data[0].price.unit_amount,
                currency: subscription.currency,
                customer: subscription.customer,
                customeremail: "",
                mode: "subscription",
                paymentintent: "",
                subscriptionid: subscription.id,
                startfrom: moment.unix(subscription.current_period_start),
                endfrom: moment.unix(subscription.current_period_end),
                paymentmethod: subscription.default_payment_method,
                invoice: subscription.latest_invoice.id,
            });

            const rafflesRounds = await RaffleRoundsModal.find({
                startingtime: {
                    $gt: Date.now()
                }
            });

            forEachAsync(rafflesRounds, async function (e) {
                let cr = e.lastno || 0;

                if (cr == 0) {
                    const batch = Math.floor(Math.random() * (9 - 1 + 1) + 1);
                    const fn = batch * 100000;
                    cr = fn + 1;
                }

                for (var i = 0; i < sub.count; i++) {
                    await UserRafflesModel.create({
                        uid: uid,
                        raffleroundid: e._id,
                        subid: sub.subid,
                        type: "subscription",
                        createddate: Date.now(),
                        entryNumber: cr,
                    });
                    cr += 1;
                }

                const round = await RaffleRoundsModal.findOne({ _id: e._id });
                round.lastno = cr;
                round.save();
            }).then(function () {
                UserModal.findOneAndUpdate({ uid: uid }, {
                    sub_id: sub.subid,
                    subscriptionid: subscription.id,
                    stripe_id: subscription.customer
                }, {
                    new: true
                }).
                    then(user => {
                        res.json({
                            status: 200,
                            data: user
                        });
                    })
                    .catch(err => {
                        res.json({
                            status: 400,
                            data: err
                        });
                    });
            });
        }
    } catch (error) {
        res.json({
            status: 500,
            message: "Internal server error"
        });

    }

});

app.post('/checkoutSession', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const subid = req.body.subid;
        const uid = req.body.uid;
        const existingUser = await UserModal.findOne({ uid: uid });

        const subscription = await functions.getSubscription(subid);

        if (existingUser?.sub_id == undefined || existingUser.sub_id == "") {
            if (subscription) {
                try {
                    const session = await stripe.checkout.sessions.create({
                        success_url: `${process.env.SELF_ADDRESS}/paymentSuccess?uid=${uid}&sub_id=${subid}&session_id={CHECKOUT_SESSION_ID}`,
                        line_items: [
                            { price: subscription.priceid, quantity: 1 },
                        ],
                        mode: 'subscription',
                        allow_promotion_codes: true
                    });
                    res.json({
                        status: 200,
                        message: 'paylink accepted?',
                        payurl: session?.url
                    });
                } catch (error) {
                    res.json({
                        status: 400,
                        message: `Stripe checkout failed. Please try again: ${error.message}`,
                        payurl: null
                    });
                }
            } else {
                res.json({
                    status: 400,
                    message: 'Subscription server not responding. Please try again!',
                    payurl: null
                });
            }
        } else {
            if (existingUser.sub_id != subid) {
                if (subscription) {
                    try {
                        const session = await stripe.checkout.sessions.create({
                            success_url: `${process.env.SELF_ADDRESS}/paymentSuccess?uid=${uid}&sub_id=${subid.toString()}&session_id={CHECKOUT_SESSION_ID}`,
                            line_items: [
                                { price: subscription.priceid, quantity: 1 },
                            ],
                            mode: 'subscription',
                            allow_promotion_codes: true
                        });
                        res.json({
                            status: 200,
                            message: 'paylink accepted?',
                            payurl: session?.url
                        });
                    } catch (error) {
                        res.json({
                            status: 400,
                            message: `Stripe checkout failed. Please try again: ${error.message}`,
                            payurl: null
                        });
                    }
                } else {
                    res.json({
                        status: 400,
                        message: 'Subscription server not responding. Please try again!',
                        payurl: null
                    });
                }
            } else {
                res.json({
                    status: 400,
                    message: 'Already Subscription Existed!',
                    payurl: null
                });
            }
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get("/paymentSuccess", async (req, res) => {
    const session = await stripe.checkout.sessions.retrieve(
        req.query.session_id, {
        expand: ['subscription'],
    }
    );

    if (session) {
        if (session.payment_status == "paid") {
            const existingUser = await UserModal.findOne({ uid: req.query.uid });

            if (existingUser.subscriptionid) {
                try {
                    await stripe.subscriptions.cancel(
                        existingUser.subscriptionid
                    );

                    await SubsciptionModel.findOneAndUpdate({ subscriptionid: existingUser.subscriptionid }, {
                        status: 0
                    }, {
                        new: true
                    })
                } catch (error) {

                }
            }

            await functions.createTransaction({
                uid: req.query.uid,
                transactiontype: "DR",
                type: "stripe",
                subid: req.query.sub_id,
                sessionid: session.id,
                amount: session.amount_total / 100,
                currency: session.currency,
                customer: session.customer,
                customeremail: session.customer_details.email,
                mode: session.mode,
                paymentintent: session.payment_intent,
                subscriptionid: session.subscription.id,
                startfrom: moment.unix(session.subscription.current_period_start),
                endfrom: moment.unix(session.subscription.current_period_end),
                paymentmethod: session.subscription.default_payment_method,
                invoice: session.subscription.latest_invoice,
            });

            const subscription = await functions.getSubscription(req.query.sub_id);

            const rafflesRounds = await RaffleRoundsModal.find({
                startingtime: {
                    $gt: Date.now()
                }
            });

            forEachAsync(rafflesRounds, async function (e) {
                let cr = e.lastno;

                for (var i = 0; i < subscription.count; i++) {
                    await UserRafflesModel.create({
                        uid: req.query.uid,
                        raffleroundid: e._id,
                        subid: req.query.sub_id,
                        type: "subscription",
                        createddate: Date.now(),
                        entryNumber: cr,
                    });
                    cr += 1;
                }

                const round = await RaffleRoundsModal.findOne({ _id: e._id });
                round.lastno = cr;
                round.save();
            }).then(async function () {

                //             try {
                //                 await functions.sendEmail(user.email, 'Confirmation of Subscription Plan Purchase', `<div><img src="https://winlads-api.com/img/win1000ban.png"></img><p>Dear ${user.firstname},</p><p>We are delighted to confirm that your purchase of the subscription plan for Winlads Giveaway Program has been successfully processed. You are now officially subscribed to our premium service!
                //                  <br>
                //                  <br>
                //                  Here are the details of your subscription plan:
                //                  - Plan Name: [Plan Name]
                //                  - Duration: [Duration]
                //                  - Start Date: [Start Date]
                //                  - Renewal Date: [Renewal Date]
                //                  - Amount Paid: [Amount]
                //                  <br>
                //                  <br>
                //                  Your subscription plan includes access to exclusive giveaways, early access to new features, and priority customer support. We hope you enjoy the benefits of your subscription.
                //                  <br>
                //                  <br>
                //                  If you have any questions or require assistance with your subscription, please feel free to reach out to our support team at support@winlads.com.
                //                  <br>
                //                  <br>
                //                  Thank you for choosing Winlads Giveaway Program. We appreciate your support and look forward to providing you with an exceptional experience.
                //                 <br><br>
                //                  Sincerely, 
                //                 <br>
                //                 <img src="https://winlads-api.com/img/50win.png"></img><br>
                //                 Winlads Team
                //   </p><div>`)
                //             } catch (error) {

                //             }

                await SubsciptionModel.create({
                    uid: existingUser.uid,
                    subid: req.query.sub_id,
                    subscriptionid: session.subscription.id,
                    type:"Stripe",
                    trial:0,
                    status: 1
                });

                UserModal.findOneAndUpdate({ uid: req.query.uid }, {
                    sub_id: req.query.sub_id,
                    subscriptionid: session.subscription.id,
                    stripe_id: session.customer,
                    trial: false,
                }, {
                    new: true
                }).
                    then(user => {
                        res.redirect(process.env.CLIENT_ADDRESS.concat(`/subscription-done?suc=1&sub_id=${req.query.sub_id}`))
                    })
                    .catch(err => {
                        res.redirect(process.env.CLIENT_ADDRESS.concat("/subscription-done?suc=0&fail=1&sub_id="))
                    });
            });
        } else {
            res.redirect(process.env.CLIENT_ADDRESS.concat("/subscription-done?suc=0&fail=2&sub_id="))
        }
    } else {
        res.redirect(process.env.CLIENT_ADDRESS.concat("/subscription-done?suc=0&fail=3&sub_id="))
    }
}, []);

app.post("/paymentSuccessCrypto", async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const existingUser = await UserModal.findOne({ uid: req.body.uid });

        if (existingUser) {
            await functions.createTransaction({
                uid: uid,
                transactiontype: "DR",
                type: "crypto",
                subid: req.body.sub_id,
                sessionid: "",
                amount: req.body.amount,
                currency: "",
                customer: "",
                customeremail: "",
                mode: "subscription",
                paymentintent: "",
                subscriptionid: "",
                startfrom: Date.now(),
                endfrom: moment().add(1, 'months'),
                paymentmethod: "",
                invoice: req.body.hash,
            });

            const subscription = await functions.getSubscription(req.body.sub_id);

            const rafflesRounds = await RaffleRoundsModal.find({
                startingtime: {
                    $gt: Date.now()
                }
            });

            forEachAsync(rafflesRounds, async function (e) {
                let cr = e.lastno;

                for (var i = 0; i < subscription.count; i++) {
                    await UserRafflesModel.create({
                        uid: req.body.uid,
                        raffleroundid: e._id,
                        subid: req.body.sub_id,
                        type: "subscription",
                        createddate: Date.now(),
                        entryNumber: cr,
                    });
                    cr += 1;
                }

                const round = await RaffleRoundsModal.findOne({ _id: e._id });
                round.lastno = cr;
                round.save();
            }).then(function () {
                UserModal.findOneAndUpdate({ uid: req.body.uid }, {
                    sub_id: req.body.sub_id,
                    trial: false,
                }, {
                    new: true
                }).
                    then(user => {
                        res.redirect(process.env.CLIENT_ADDRESS.concat(`/subscription-done?suc=1&sub_id=${req.body.sub_id}`))
                    })
                    .catch(err => {
                        res.redirect(process.env.CLIENT_ADDRESS.concat("/subscription-done?suc=0&fail=1&sub_id="))
                    });
            });
        } else {
            res.json({
                status: 401,
                data: {},
                message: "No User",
            });
        }
    } catch (error) {
        res.status(500).json({ message: 'Internal Server Error' });
    }
}, []);

app.post('/unsubscribe', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const existingUser = await UserModal.findOne({ uid: req.body.uid });

        if (existingUser.subscriptionid) {
            try {
                await stripe.subscriptions.cancel(
                    existingUser.subscriptionid
                );

                await SubsciptionModel.create({
                    uid: existingUser.uid,
                    subid: existingUser.sub_id,
                    subscriptionid: existingUser.subscriptionid,
                    status: 0
                });

                UserModal.findOneAndUpdate({ uid: req.body.uid }, { $unset: { sub_id: existingUser.sub_id, subscriptionid: existingUser.subscriptionid } }).
                    then(user => {
                        res.json({
                            status: 200,
                            data: user,
                            message: "Unsubscribed",
                        });
                    })
                    .catch(err => {
                        res.json({
                            status: 401,
                            data: {},
                            message: "Failed to Unsubscribe",
                        });
                    });
            } catch (error) {
                res.json({
                    status: 400,
                    data: {},
                    message: "Failed to Unsubscribe",
                });
            }
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.post('/subscribeWithPoints', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const subid = req.body.subid;
        const uid = req.body.uid;
        const existingUser = await UserModal.findOne({ uid: uid });

        const subscripton = await functions.getSubscription(subid);

        if (subscripton) {
            if (existingUser?.sub_id == subid) {
                res.json({
                    status: 400,
                    message: 'Subscription Already Existed!',
                    payurl: null
                });
            } else {
                if (existingUser.balance > subscripton.price) {
                    await functions.createTransaction({
                        uid: uid,
                        transactiontype: "DR",
                        type: "balance",
                        subid: subid,
                        sessionid: "",
                        amount: subscripton.price,
                        currency: "",
                        customer: "",
                        customeremail: "",
                        mode: "subscription",
                        paymentintent: "",
                        subscriptionid: "",
                        startfrom: Date.now(),
                        endfrom: moment().add(1, 'months'),
                        paymentmethod: "",
                        invoice: "",
                    });

                    await UserModal.findOneAndUpdate({ uid: uid }, {
                        sub_id: subid,
                        trial: false,
                    }, {
                        new: true
                    }).
                        then(user => {
                            res.json({
                                status: 200,
                                data: {
                                    success: true,
                                    message: 'Subscribed'
                                }
                            });
                        })
                        .catch(err => {
                            res.json({
                                status: 200,
                                data: {
                                    success: false,
                                    message: 'User update failed'
                                }
                            });
                        });
                } else {
                    res.json({
                        status: 400,
                        data: {
                            success: false,
                            message: 'Insufficient balance'
                        }
                    });
                }
            }
        } else {
            res.json({
                status: 400,
                message: 'Check subscription id',
                payurl: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getSubscription', async (req, res) => {
    try {
        const uid = req.query.uid;
        const existingSubscription = await SubsciptionModel.findOne({ uid: uid });
        if (existingSubscription) {
            res.json({
                "status": 200,
                exists: true,
                data: existingSubscription
            });
        } else {
            res.json({
                "status": 400,
                exists: false,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

//add Raffles
app.post("/addRaffle", async (req, res) => {
    try {
        const { name, date, type, image, color } = req.body;
        const newRafflesModel = new RafflesModal(
            req.body
        );

        const savednewRaffles = await newRafflesModel.save();

        res.json({
            status: 200,
            data: savednewRaffles,
            message: "Raffles added successfully",
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

// Update Raffles
app.put("/updateRaffle/:id", async (req, res) => {
    try {
        const { name, date, type, image, color, raffleimage } = req.body;

        const updateRaffles =
            await RafflesModal.findByIdAndUpdate(
                req.params.id,
                {
                    $set: {
                        name,
                        date,
                        type,
                        image,
                        raffleimage,
                        color,
                    },
                },
                { new: true }
            );

        if (updateRaffles) {
            res.json({
                status: 200,
                data: updateRaffles,
                message: "Raffles updated successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "Raffles not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

// Delete Raffles
app.delete("/deleteRaffle/:id", async (req, res) => {
    try {
        const status = 0;
        const deleteRaffles =
            await RafflesModal.findByIdAndUpdate(
                req.params.id,
                {
                    $set: {
                        status,
                    },
                },
                { new: true }
            );

        if (deleteRaffles) {
            res.json({
                status: 200,
                data: deleteRaffles,
                message: "Raffles deleted successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "Raffles not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

app.get('/raffles', async (req, res) => {
    try {
        const raffels = await RafflesModal.find();
        if (raffels) {
            res.json({
                "status": 200,
                data: raffels
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});


app.get('/getRaffle', async (req, res) => {
    try {
        const id = req.query.id;
        const raffle = await RafflesModal.findOne({ _id: id });
        if (raffle) {
            res.json({
                "status": 200,
                data: raffle
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/raffleRounds', async (req, res) => {
    const start = req.query.start || '';
    const end = req.query.end || '';
    try {
        const selectedRaffelsRounds = await RaffleRoundsModal.find({
            raffleid: req.query.raffleid, startingtime: {
                $gte: Date.now(),
            }
        }).sort({ startingtime: -1 })

        if (selectedRaffelsRounds) {
            res.json({
                "status": 200,
                data: selectedRaffelsRounds
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/raffleRoundTickets', async (req, res) => {
    try {
        const tickets = await UserRafflesModel.find({
            raffleroundid: req.query.roundid
        }).sort({ createddate: -1 });

        if (tickets) {
            let ticketlist = [];
            forEachAsync(tickets, async function (e) {
                let data = e;
                const round = await RaffleRoundsModal.findOne({ _id: e.raffleroundid });
                let raffle;
                if (round) {
                    raffle = await RafflesModal.findOne({ _id: round.raffleid });
                }
                const user = await UserModal.findOne({ uid: e.uid });

                data = { ...data._doc, round, raffle, user };
                ticketlist.push(data);
            }).then(function () {
                res.json({
                    "status": 200,
                    data: ticketlist,
                    count: ticketlist.length
                });
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/raffleRoundTicketsByUsers', async (req, res) => {
    try {
        const limitFrom = req.query.from || 1;
        const limitTo = req.query.to || 5;
        const name = req.query.name || '';
        const mobile = req.query.mobile || '';
        const email = req.query.email || '';

        let conditions = [];

        if (name != '') {
            conditions.push({ 'condition': 'firstname' })
        }
        if (mobile != '') {
            conditions.push({ 'condition': 'mobile' })
        }
        if (email != '') {
            conditions.push({ 'condition': 'email' })
        }

        const tickets = await UserRafflesModel.aggregate([
            {
                $match: {
                    raffleroundid: req.query.roundid
                },
            },
            {
                $group: {
                    _id: '$uid',
                    entryCount: { $sum: 1 } // this means that the count will increment by 1
                }
            }
        ]);

        if (tickets) {
            const round = await RaffleRoundsModal.findOne({ _id: req.query.roundid });
            let raffle;
            if (round) {
                raffle = await RafflesModal.findOne({ _id: round.raffleid });
            }
            let ticketlist = [];
            let index = 1;
            forEachAsync(tickets, async function (e) {
                let push = true;
                let data = e;

                const user = await UserModal.findOne({ uid: e._id });

                forEachAsync(conditions, async function (condition) {
                    if (condition.condition == 'name' && !(user.firstname == name)) {
                        push = false;
                    } else if (condition.condition == 'email' && !(user.email == email)) {
                        push = false;
                    } else if (condition.condition == 'mobile' && !(user.mobile == '+' + mobile)) {
                        push = false;
                    }
                }).then(function () {
                    if (push) {
                        data = { user, entries: e.entryCount };

                        if (limitFrom <= index && index <= limitTo) {
                            ticketlist.push(data);
                        }
                        index++;
                    }
                });

            }).then(function () {
                res.json({
                    "status": 200,
                    data: ticketlist,
                    count: index - 1,
                    raffle: raffle,
                    round: round
                });
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/raffleRoundTicketsByUser', async (req, res) => {
    try {
        const tickets = await UserRafflesModel.find({
            raffleroundid: req.query.roundid,
            uid: req.query.uid
        }).sort({ createddate: -1 });

        if (tickets) {
            let ticketlist = [];
            forEachAsync(tickets, async function (e) {
                let data = e;
                const round = await RaffleRoundsModal.findOne({ _id: e.raffleroundid });
                let raffle;
                if (round) {
                    raffle = await RafflesModal.findOne({ _id: round.raffleid });
                }
                const user = await UserModal.findOne({ uid: e.uid });

                data = { ...data._doc, round, raffle, user };
                ticketlist.push(data);
            }).then(function () {
                res.json({
                    "status": 200,
                    data: ticketlist,
                    count: ticketlist.length
                });
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/raffleRoundsAll', async (req, res) => {
    try {
        const selectedRaffelsRounds = await RaffleRoundsModal.find({
            raffleid: req.query.raffleid
        });

        if (selectedRaffelsRounds) {
            res.json({
                "status": 200,
                data: selectedRaffelsRounds
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/raffleRoundsAllCategories', async (req, res) => {
    try {
        const selectedRaffelsRounds = await RaffleRoundsModal.find();

        if (selectedRaffelsRounds) {
            res.json({
                "status": 200,
                data: selectedRaffelsRounds
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/raffleRoundsPast', async (req, res) => {
    try {
        const selectedRaffelsRounds = await RaffleRoundsModal.find({
            startingtime: {
                $lt: Date.now(),
            }
        });
        if (selectedRaffelsRounds) {
            const raffles = await RafflesModal.find();
            let rounds = [];
            selectedRaffelsRounds.forEach(async (e, i) => {
                let data = e;
                const raffle = raffles.find((v) => v._id == e.raffleid);
                data = { ...data._doc, raffle };
                rounds.push(data);
            });

            res.json({
                "status": 400,
                data: rounds
            });
        } else {
            res.json({
                "status": 400,
                data: { message: "No rounds" }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/raffleRoundsActive', async (req, res) => {
    try {
        const selectedRaffelsRounds = await RaffleRoundsModal.find({
            startingtime: {
                $gt: Date.now(),
            },
            roundStatus: 2
        });
        if (selectedRaffelsRounds) {
            const raffles = await RafflesModal.find();
            let rounds = [];
            selectedRaffelsRounds.forEach(async (e, i) => {
                let data = e;
                const raffle = raffles.find((v) => v._id == e.raffleid);
                data = { ...data._doc, raffle };
                rounds.push(data);
            });

            res.json({
                "status": 400,
                data: rounds
            });
        } else {
            res.json({
                "status": 400,
                data: { message: "No rounds" }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/raffleRoundsUpcoming', async (req, res) => {
    try {
        const selectedRaffelsRounds = await RaffleRoundsModal.find({
            startingtime: {
                $gt: Date.now(),
            }
        });
        if (selectedRaffelsRounds) {
            const raffles = await RafflesModal.find();
            let rounds = [];
            selectedRaffelsRounds.forEach(async (e, i) => {
                let data = e;
                const raffle = raffles.find((v) => v._id == e.raffleid);
                data = { ...data._doc, raffle };
                rounds.push(data);
            });

            res.json({
                "status": 200,
                data: rounds
            });
        } else {
            res.json({
                "status": 400,
                data: { message: "No rounds" }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});



app.get('/myRaffleRounds', async (req, res) => {
    const limitFrom = req.query.from || 1;
    const limitTo = req.query.to || 10;

    const category = req.query.category || '';
    const roundid = req.query.round || '';
    const numbers = req.query.numbers || '';
    const date = req.query.date || '';
    const winstatus = req.query.status || '';

    let conditions = [];

    if (category != '') {
        conditions.push({ 'condition': 'category' })
    }
    if (roundid != '') {
        conditions.push({ 'condition': 'roundid' })
    }
    if (numbers != '') {
        conditions.push({ 'condition': 'numbers' })
    }
    if (date != '') {
        conditions.push({ 'condition': 'date' })
    }
    if (winstatus != '') {
        conditions.push({ 'condition': 'status' })
    }

    try {
        //get all raffles
        let selectedRaffelsRounds;

        selectedRaffelsRounds = await UserRafflesModel.find({
            uid: req.query.uid,
        }).sort({ createddate: -1 });

        if (selectedRaffelsRounds) {
            let futuretickets = [];
            let pasttickets = [];
            let index = 1;
            let active = 0;
            let winstatus = 'pending';

            forEachAsync(selectedRaffelsRounds, async function (e) {
                let push = true;
                let data = e;

                const round = await RaffleRoundsModal.findOne({ _id: e.raffleroundid });
                let raffle;
                if (round) {
                    raffle = await RafflesModal.findOne({ _id: round.raffleid });
                }

                forEachAsync(conditions, async function (condition) {
                    if (condition.condition == 'category' && !(raffle && raffle.type == category)) {
                        push = false;
                    } else if (condition.condition == 'roundid' && !(e.raffleroundid == roundid)) {
                        push = false;
                    } else if (condition.condition == 'numbers' && !(e.entryNumber == numbers)) {
                        push = false;
                    } else if (condition.condition == 'date' && !(moment(e.createddate).format('DD-MM-YYYY') == moment(date).format('DD-MM-YYYY'))) {
                        push = false;
                    } else if (condition.condition == 'status') {
                        if (winstatus == 'win' && !(e.entryNumber == round.winningNumber)) {
                            push = false;
                        } else if (winstatus == 'lost' && e.entryNumber == round.winningNumber) {
                            push = false;
                        } else if (winstatus == 'pending' && round.winningNumber) {
                            push = false;
                        }
                    }

                }).then(function () {
                    if (push) {
                        if (round?.startingtime > Date.now()) {
                            active += 1;
                        }
                        if (limitFrom <= index && index <= limitTo) {
                            if (round?.winningNumber && e.entryNumber == round?.winningNumber) {
                                winstatus = 'win';
                            } else if (round?.winningNumber && e.entryNumber != round?.winningNumber) {
                                winstatus = 'lost';
                            } else {
                                winstatus = 'pending';
                            }

                            data = { ...data._doc, round, raffle, winstatus };
                            if (round?.startingtime > Date.now()) {
                                futuretickets.push(data);
                            } else {
                                pasttickets.push(data);
                            }

                        }
                        index++;
                    }
                });
            }).then(function () {
                res.json({
                    "status": 200,
                    data: {
                        future: futuretickets,
                        past: pasttickets,
                        count: index - 1,
                        activeRounds: active
                    }
                });
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getRaffleRound', async (req, res) => {
    try {
        const raffelsRound = await RaffleRoundsModal.findOne({ _id: req.query.raffleroundid });
        if (raffelsRound) {
            res.json({
                "status": 200,
                data: raffelsRound
            });
        } else {
            res.json({
                "status": 400,
                data: null
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getLiveRaffleRound', async (req, res) => {
    try {
        const liveRaffle = await RaffleRoundsModal.findOne({
            startingtime: {
                $lt: Date.now(),
            },
            endtime: {
                $gt: Date.now(),
            },
        });

        if (liveRaffle) {
            res.json({
                "status": 200,
                data: liveRaffle
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No live raffles' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.post('/buyRaffleRoundWithPoints', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const roundid = req.body.roundid;
        const uid = req.body.uid;
        const user = await UserModal.findOne({ uid: uid });

        if (user) {
            const Round = await RaffleRoundsModal.findOne({ _id: roundid });
            if (Round.price <= user.balance) {
                await functions.createTransaction({
                    uid: uid,
                    transactiontype: "DR",
                    type: "balance",
                    roundid: roundid,
                    sessionid: "",
                    amount: Round.price,
                    currency: "",
                    customer: "",
                    customeremail: "",
                    mode: "payment",
                    paymentintent: "",
                    subscriptionid: "",
                    startfrom: Date.now(),
                    endfrom: Date.now(),
                    paymentmethod: "",
                    invoice: "",
                });

                const entryNo = Round.lastno + 1;
                Round.lastno = entryNo + 1;
                Round.save();

                await UserRafflesModel.create({
                    uid: uid,
                    raffleroundid: roundid,
                    status: 1,
                    subid: "",
                    type: "points",
                    createddate: Date.now(),
                    entryNumber: entryNo
                }).then(raffle => {
                    res.json({
                        status: 200,
                        message: "Giveaway successfully purchased",
                        data: raffle
                    });
                }).catch(err => {
                    console.log("er", err)
                    res.json({
                        "status": 400,
                        "data": err
                    })
                })
            } else {
                res.json({
                    "status": 400,
                    data: { message: 'Insufficient balance' }
                });
            }
        } else {
            res.json({
                "status": 400,
                data: { message: 'No User' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.post('/buyRaffleRoundWithPayment', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const roundid = req.body.roundid;
        const uid = req.body.uid;
        const count = req.body.count || 1;
        const user = await UserModal.findOne({ uid: uid });

        if (user) {
            const Round = await RaffleRoundsModal.findOne({ _id: roundid });
            let price = (Round.price * 100) * count;
            if (req.body.coupen && req.body.coupen === 'WIN50OFF') {
                price = price / 2;
            }

            try {
                const session = await stripe.checkout.sessions.create({
                    success_url: `${process.env.SELF_ADDRESS}/rafflePaymentSuccess?uid=${uid}&roundid=${roundid}&count=${count}&session_id={CHECKOUT_SESSION_ID}`,
                    line_items: [
                        {
                            price_data: {
                                currency: "aud",
                                unit_amount: price,
                                product_data: {
                                    name: Round.name,
                                },
                            },
                            quantity: 1
                        },
                    ],
                    mode: 'payment',
                    allow_promotion_codes: true
                });
                res.json({
                    status: 200,
                    message: 'paylink accepted',
                    payurl: session?.url
                });
            } catch (error) {
                res.json({
                    status: 400,
                    message: `Stripe checkout failed. Please try again: ${error.message}`,
                    payurl: null
                });
            }
        } else {
            res.json({
                "status": 400,
                data: { message: 'No User' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get("/rafflePaymentSuccess", async (req, res) => {
    const session = await stripe.checkout.sessions.retrieve(
        req.query.session_id, {
        expand: ['payment_intent'],
    }
    );

    if (session) {
        if (session.payment_status == "paid") {
            try {
                const paymentIntentId = session.payment_intent ? session.payment_intent.id : null;

                await functions.createTransaction({
                    uid: req.query.uid,
                    transactiontype: "DR",
                    type: "stripe",
                    roundid: req.query.roundid,
                    sessionid: session.id,
                    amount: session.amount_total / 100,
                    currency: session.currency,
                    customer: session.customer,
                    customeremail: session.customer_details.email,
                    mode: session.mode,
                    paymentintent: paymentIntentId, // Use the variable here
                    subscriptionid: "",
                    startfrom: Date.now(),
                    endfrom: Date.now(),
                    paymentmethod: "",
                    invoice: "",
                });

                const Round = await RaffleRoundsModal.findOne({ _id: req.query.roundid });
                const count = req.query.count || 1;
                const entryNo = Round.lastno;

                const counts = Array.from(
                    { length: (count - 1) / 1 + 1 }, (value, index) => 1 + index * 1
                );

                forEachAsync(counts, async function (count) {

                    await UserRafflesModel.create({
                        uid: req.query.uid,
                        raffleroundid: req.query.roundid,
                        status: 1,
                        subid: "",
                        type: "cash",
                        createddate: Date.now(),
                        entryNumber: entryNo + count
                    });
                }).then(function () {
                    Round.lastno = entryNo + counts.length;
                    Round.save();
                    res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-done?suc=1&round_id=${req.query.roundid}`))
                });
            } catch (error) {
                res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-done?suc=0&fail=1&round_id=${req.query.roundid}`))
            }
        } else {
            res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-done?suc=0&fail=2&round_id=${req.query.roundid}`))
        }
    } else {
        res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-done?suc=0&fail=3&round_id=${req.query.roundid}`))
    }
}, []);

app.post("/rafflePaymentSuccessCrypto", async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        await functions.createTransaction({
            uid: req.body.uid,
            transactiontype: "DR",
            type: "crypto",
            roundid: req.query.roundid,
            sessionid: "",
            amount: req.body.amount,
            currency: "",
            customer: "",
            customeremail: "",
            mode: "payment",
            paymentintent: "",
            subscriptionid: "",
            startfrom: Date.now(),
            endfrom: Date.now(),
            paymentmethod: "",
            invoice: req.body.hash,
        });

        const Round = await RaffleRoundsModal.findOne({ _id: req.body.roundid });
        const entryNo = Round.lastno + 1;

        const count = req.body.count || 1;
        const counts = Array.from(
            { length: (count - 1) / 1 + 1 }, (value, index) => 1 + index * 1
        );

        forEachAsync(counts, async function (count) {
            const entryNo = Round.lastno + count;
            await UserRafflesModel.create({
                uid: req.body.uid,
                raffleroundid: req.body.roundid,
                status: 1,
                subid: "",
                type: "crypto",
                createddate: Date.now(),
                entryNumber: entryNo
            });
        }).then(function () {
            Round.lastno = entryNo + counts.length;
            Round.save();
            res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-done?suc=1&round_id=${req.body.roundid}`))
        });
    } catch (error) {
        res.redirect(process.env.CLIENT_ADDRESS.concat(`/payment-done?suc=0&fail=4&round_id=${req.body.roundid}`))
    }
}, []);

//add Faq
app.post("/addFaq", async (req, res) => {
    try {
        const faq = await FaqModel.create(req.body);

        if (faq) {
            res.json({
                status: 200,
                data: faq,
                message: "Faq added successfully",
            });
        } else {
            res.json({
                status: 401,
                data: {},
                message: "Failed to add Faq",
            });
        }

    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

//update Faq
app.put("/updateFaq/:id", async (req, res) => {
    try {
        const { q, a } = req.body;
        const updatedFaq = await FaqModel.findByIdAndUpdate(
            req.params.id,
            {
                $set: {
                    q,
                    a,
                },
            },
            { new: true }
        );

        if (updatedFaq) {
            res.json({
                status: 200,
                data: updatedFaq,
                message: "Faq updated successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "Faq not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

//Delete Faq
app.delete("/deleteFaq/:id", async (req, res) => {
    try {
        const status = 0;
        const updatedFaq = await FaqModel.findByIdAndUpdate(
            req.params.id,
            {
                $set: {
                    status,
                },
            },
            { new: true }
        );

        if (updatedFaq) {
            res.json({
                status: 200,
                data: updatedFaq,
                message: "Faq deleted successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "Faq not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

app.get('/getFaqs', async (req, res) => {
    try {
        const faq = await FaqModel.find({ status: 1 });

        if (faq) {
            res.json({
                "status": 200,
                data: faq
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No faqs' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getFaq', async (req, res) => {
    try {
        const faq = await FaqModel.findOne({ _id: req.query.id });

        if (faq) {
            res.json({
                "status": 200,
                data: faq
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No faqs' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getNews', async (req, res) => {
    try {
        const news = await NewsModel.find();

        if (news) {
            res.json({
                "status": 200,
                data: news
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No News' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getSingleNews', async (req, res) => {
    try {
        const news = await NewsModel.findOne({ _id: req.query.id });

        if (news) {
            res.json({
                "status": 200,
                data: news
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No News' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getBusinessCard', async (req, res) => {
    try {
        const user = await UserModal.findOne({ uid: req.query.uid });

        const text = `uid - ${user?.uid}\nname - ${user?.name}\nmoble - ${user?.mobile}\naddress - ${user?.address}\n`;

        if (user) {
            var bucket = getStorage().bucket();
            const file = bucket.file(req.query.uid + '.png');

            file.exists()
                .then(async (exists) => {
                    if (exists[0]) {
                        console.log("File exists");
                    } else {
                        const qr = await QRCode.toDataURL(text);
                        let bufferStream = new stream.PassThrough();
                        bufferStream.end(new Buffer.from(qr.split(',')[1], 'base64'));

                        bufferStream.pipe(file.createWriteStream({
                            metadata: {
                                contentType: 'image/png'
                            }
                        }))
                            .on('error', error => {

                            })
                            .on('finish', (file) => {

                            });
                    }
                })

            const card = await BusinessCardModel.findOne({ uid: user.uid });

            if (card) {
                const link = await file.getSignedUrl({
                    action: 'read',
                    expires: '03-09-2491'
                });
                res.json({
                    "status": 200,
                    data: {
                        name: card.name,
                        mobile: card.mobile,
                        passport: card.passport,
                        address: card.address,
                        address2: user.address2,
                        city: card.city,
                        state: card.state,
                        postalcode: card.postalcode,
                        uid: card.uid,
                        image: link
                    }
                });
            } else {
                await BusinessCardModel.create({
                    uid: user.uid,
                    name: user.name,
                    mobile: user.mobile,
                    passport: user.passport,
                    address: user.address,
                    address2: user.address2,
                    city: user.city,
                    state: user.state,
                    postalcode: user.postalcode,
                    qr: user.uid + '.png',
                });
                res.json({
                    "status": 200,
                    data: {
                        name: user.name,
                        mobile: user.mobile,
                        passport: user.passport,
                        address: user.address,
                        address2: user.address2,
                        city: user.city,
                        state: user.state,
                        postalcode: user.postalcode,
                        uid: user.uid,
                        image: user.uid + '.png'
                    }
                });
            }
        } else {
            res.json({
                "status": 401,
                data: { message: 'No Card' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.post('/requestBusinessCard', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const uid = req.body.uid;
        const user = await UserModal.findOne({ uid: uid });

        if (user) {
            const card = await BusinessCardModel.findOne({ uid: uid });
            if (!card) {
                await BusinessCardModel.create({ uid: uid });
            }

            await BusinessCardModel.findOneAndUpdate({ uid: uid }, {
                name: req.body.name,
                mobile: req.body.mobile,
                passport: req.body.passport,
                address: req.body.address,
                address2: req.body.address2,
                city: req.body.city,
                state: req.body.state,
                postalcode: req.body.postalcode,
            }, {
                new: true
            }).
                then(card => {
                    //email sending should code here
                    functions.sendEmail('admin@winlads.com', 'Winlads Business Card Request', `<p>Hi Admin,</p>
                    <p>${user.firstname + ' ' + user.lastname} requested a business card. Here are the details:</p>
                    <ul>
                      <li><strong>Name:</strong> ${user.firstname} ${user.lastname}</li>
                      <li><strong>Email:</strong> ${user.email}</li>
                      <li><strong>Address:</strong> ${req.body.address}</li>
                    </ul>
                    <p>Thank you,</p>`)
                    res.json({
                        status: 200,
                        message: "Request sent successfully",
                        data: card
                    });
                })
                .catch(err => {
                    console.log("er", err)
                    res.json({
                        "status": 400,
                        "data": err
                    })
                });
        } else {
            res.json({
                "status": 400,
                data: { message: 'No User' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getPointBalances', async (req, res) => {
    try {
        const user = await UserModal.findOne({ uid: req.query.uid });
        const earings = await TransactionModel.find({ uid: req.query.uid, type: 'balance', transactiontype: 'CR' });
        const purchase = await TransactionModel.find({ uid: req.query.uid, transactiontype: 'DR' });
        let psum = 0.00;
        purchase.forEach((e) => {
            psum += parseFloat(e.amount);
        });

        let esum = 0.00;
        earings.forEach((e) => {
            esum += parseFloat(e.amount);
        });

        if (user) {
            res.json({
                "status": 200,
                data: {
                    balance: user.balance,
                    earning: parseFloat(esum).toFixed(2),
                    purchase: parseFloat(psum).toFixed(2)
                }
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No Card' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getTransactions', async (req, res) => {
    try {
        const uid = req.query.uid;
        const start = req.query.start || '';
        const end = req.query.end || '';
        const type = req.query.type || '';
        const limitFrom = req.query.from || 1;
        const limitTo = req.query.to || 10;

        let transactions;
        if (start != '' && end != '') {
            transactions = await TransactionModel.find({
                uid: uid, startfrom: {
                    $gte: start,
                    $lte: end,
                }
            });
        } else if (start == '' && end != '') {
            transactions = await TransactionModel.find({
                uid: uid, startfrom: {
                    $lte: end,
                }
            });
        } else if (start != '' && end == '') {
            transactions = await TransactionModel.find({
                uid: uid, startfrom: {
                    $gte: start,
                }
            });
        } else {
            transactions = await TransactionModel.find({
                uid: uid
            });
        }

        let trans = [];
        let index = 1;

        forEachAsync(transactions, async function (transaction) {
            if (type == 'card' && transaction.type == 'stripe') {
                if (limitFrom <= index && index <= limitTo) {
                    trans.push(transaction);
                }
            } else if (type == 'digital') {
            } else if (type == 'earning' && transaction.type == 'balance') {
                if (limitFrom <= index && index <= limitTo) {
                    trans.push(transaction);
                }
            } else if (type == '') {
                if (limitFrom <= index && index <= limitTo) {
                    trans.push(transaction);
                }
            }
            index++;
        }).then(function () {
            if (trans) {
                res.json({
                    "status": 200,
                    data: trans
                });
            } else {
                res.json({
                    "status": 401,
                    data: { message: 'No Transaction' }
                });
            }

        });


    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getTransactionsDR', async (req, res) => {
    try {
        const uid = req.query.uid;
        const start = req.query.start || '';
        const end = req.query.end || '';

        let transactions;
        if (start != '' && end != '') {
            transactions = await TransactionModel.find({
                transactiontype: 'DR',
                uid: uid, startfrom: {
                    $gte: start,
                    $lte: end,
                }
            });
        } else if (start == '' && end != '') {
            transactions = await TransactionModel.find({
                transactiontype: 'DR',
                uid: uid, startfrom: {
                    $lte: end,
                }
            });
        } else if (start != '' && end == '') {
            transactions = await TransactionModel.find({
                transactiontype: 'DR',
                uid: uid, startfrom: {
                    $gte: start,
                }
            });
        } else {

            transactions = await TransactionModel.find({
                transactiontype: 'DR',
                uid: uid
            });
        }

        if (transactions) {
            res.json({
                "status": 200,
                data: transactions
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No Transaction' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getTransactionsCR', async (req, res) => {
    try {
        const uid = req.query.uid;
        const start = req.query.start || '';
        const end = req.query.end || '';

        let transactions;
        if (start != '' && end != '') {
            transactions = await TransactionModel.find({
                transactiontype: 'CR',
                uid: uid, startfrom: {
                    $gte: start,
                    $lte: end,
                }
            });
        } else if (start == '' && end != '') {
            transactions = await TransactionModel.find({
                transactiontype: 'CR',
                uid: uid, startfrom: {
                    $lte: end,
                }
            });
        } else if (start != '' && end == '') {
            transactions = await TransactionModel.find({
                transactiontype: 'CR',
                uid: uid, startfrom: {
                    $gte: start,
                }
            });
        } else {
            transactions = await TransactionModel.find({
                transactiontype: 'CR',
                uid: uid
            });
        }

        if (transactions) {
            res.json({
                "status": 200,
                data: transactions
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No Transaction' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/getTransaction', async (req, res) => {
    try {
        const trans = await TransactionModel.findOne({ _id: req.query.id });
        if (trans) {
            res.json({
                "status": 200,
                data: trans
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No Transaction' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.post('/requestFundTransfer', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const uid = req.body.uid;
        const user = await UserModal.findOne({ uid: uid });

        if (user) {
            if (user.balance >= req.body.amount) {
                await functions.createTransaction({
                    uid: uid,
                    transactiontype: "DR",
                    type: "balance",
                    subid: "",
                    sessionid: "",
                    amount: req.body.amount,
                    currency: "",
                    customer: "",
                    customeremail: "",
                    mode: "transfer",
                    paymentintent: "",
                    subscriptionid: "",
                    startfrom: Date.now(),
                    endfrom: Date.now(),
                    paymentmethod: "",
                    invoice: "",
                });

                functions.sendEmail(user.email, 'Fund Transfer  Request', `<p>Hi ${user.name},</p><p>Fund transfered succesfully</p><p>Bank - ${req.body.bank}</p><p>Account Number - ${req.body.accountnumber}</p><p>Name - ${req.body.name}</p><p>Purpose - ${req.body.purpose}</p><p>Amount - ${req.body.amount}</p><p>Thank You</p>`)

                await BalanceTransferModel.create({
                    uid: uid,
                    method: req.body.method,
                    bank: req.body.bank,
                    accountnumber: req.body.accountnumber,
                    name: req.body.name,
                    purpose: req.body.purpose,
                    amount: req.body.amount,
                    bnb: req.body.bnb,
                    status: 'REQ',
                    createdat: Date.now(),
                }).
                    then(balance => {
                        //email sending should code here
                        res.json({
                            status: 200,
                            data: balance
                        });
                    })
                    .catch(err => {
                        console.log("er", err)
                        res.json({
                            "status": 400,
                            "data": err
                        })
                    });
            } else {
                res.json({
                    "status": 400,
                    data: { message: 'Insufficient balance' }
                });
            }
        } else {
            res.json({
                "status": 400,
                data: { message: 'No User' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.post('/sendNotification', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const notification = await NotificationsModel.create({
            image: req.body.image,
            title: req.body.title,
            subtitle: req.body.subtitle,
            desc: req.body.desc,
            channel: req.body.channel,
            createdat: Date.now(),
        });

        const registrationToken = 'c8t3v4TMQ0iSpd9nWh5H68:APA91bFtBumnhRyRhQEk_xt2wrb4y9YZwT1MwxitXDKXVn7ozX3hcZH0Mj6JhP_1vThjJ6AAGxlI6Yh-gx47-uUIjPlYgAdm4_qRwjBCeagnx-a-tFQZLEIUwIw_gmnjdT2sI4wcdm0w';

        const message = {
            data: {
                id: notification._id,
            },
            notification: {
                title: notification.title,
                body: notification.desc
            },
            token: registrationToken
        };

        firebase.messaging().send(message)
            .then((response) => {
                res.json({
                    status: 200,
                    data: notification
                });
            })
            .catch((error) => {
                res.json({
                    "status": 400,
                    "data": error
                })
            });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/notifications', async (req, res) => {
    try {
        const not = await NotificationsModel.find();

        if (not) {
            res.json({
                "status": 200,
                data: not
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No Card' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/notification', async (req, res) => {
    try {
        const not = await NotificationsModel.findOne({ _id: req.query.id });

        if (not) {
            res.json({
                "status": 200,
                data: not
            });
        } else {
            res.json({
                "status": 401,
                data: { message: 'No Card' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/shareBusinessCard', async (req, res) => {
    try {
        const user = await UserModal.findOne({ uid: req.query.uid });

        if (user) {
            try {
                const pdf = await functions.createPdfBuffer(user.uid);
                const resp = await functions.sendEmail(user.email, 'Winlads Business Card', `<p>Hi ${user.name}</p><p>Please find attached pdf.</p><p>Thank you</p>`, user.uid + '.pdf', pdf)
                res.json({
                    "status": 200,
                    exists: resp,
                });
            } catch (error) {
                res.json({
                    "status": 401,
                    exists: error,
                });
            }

        } else {
            res.json({
                "status": 401,
                data: { message: 'No Card' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/downloadBusinessCard', async (req, res) => {
    try {
        const user = await UserModal.findOne({ uid: req.query.uid });

        if (user) {
            try {
                const link = await functions.createPdf(user.uid);

                res.json({
                    "status": 200,
                    link: link,
                });
            } catch (error) {
                res.json({
                    "status": 401,
                    exists: error,
                });
            }

        } else {
            res.json({
                "status": 401,
                data: { message: 'No Card' }
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

//add masterDataCategory
app.post("/addMasterDataCategory", async (req, res) => {
    try {
        const masterDataCategoryModel = await MasterDataCategoryModel.create(req.body);

        if (masterDataCategoryModel) {
            res.json({
                status: 200,
                data: masterDataCategoryModel,
                message: "MasterDataCategory added successfully",
            });
        } else {
            res.json({
                status: 401,
                data: masterDataCategoryModel,
                message: "failed to create MasterDataCategory",
            });
        }

    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

//get masterDataCategory
app.get("/getMasterDataCategories", async (req, res) => {
    try {
        const not = await MasterDataCategoryModel.find({ status: 1 });

        if (not) {
            res.json({
                status: 200,
                data: not,
                message: "MasterDataCategory received successfully",
            });
        } else {
            res.json({
                status: 401,
                data: { message: "No Card" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

//get masterDataCategory by id
app.get("/getMasterDataCategory", async (req, res) => {
    try {
        const masterDataCategory = await MasterDataCategoryModel.findOne({
            _id: req.query.id,
        });

        if (masterDataCategory) {
            res.json({
                status: 200,
                data: masterDataCategory,
            });
        } else {
            res.json({
                status: 401,
                data: { message: "No masterDataCategorys" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});



// Update masterDataCategory
app.put("/updateMasterDataCategory/:id", async (req, res) => {
    try {
        const { name, description } = req.body;
        const updatedMasterDataCategory =
            await MasterDataCategoryModel.findByIdAndUpdate(
                req.params.id,
                {
                    $set: {
                        name,
                        description,
                    },
                },
                { new: true }
            );

        if (updatedMasterDataCategory) {
            res.json({
                status: 200,
                data: updatedMasterDataCategory,
                message: "MasterDataDetails updated successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "MasterDataDetails not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

// Delete masterDataCategory
app.delete("/deleteMasterDataCategory/:id", async (req, res) => {
    try {
        const status = 0;
        const deleteMasterDataCategory =
            await MasterDataCategoryModel.findByIdAndUpdate(
                req.params.id,
                {
                    $set: {
                        status,
                    },
                },
                { new: true }
            );

        if (deleteMasterDataCategory) {
            res.json({
                status: 200,
                data: deleteMasterDataCategory,
                message: "MasterDataCategory deleted successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "MasterDataCategory not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

// get masterDataDetails
app.get("/masterDataDetails", async (req, res) => {
    try {
        const not = await MasterDataDetailsModel.find();

        if (not) {
            res.json({
                status: 200,
                data: not,
                message: "MasterDataDetails received successfully",
            });
        } else {
            res.json({
                status: 401,
                data: { message: "No Card" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

//get masterDataDetail by id
app.get("/masterDataDetail", async (req, res) => {
    try {
        const masterDataDetail = await MasterDataDetailsModel.findOne({
            _id: req.query.id,
        });

        if (masterDataDetail) {
            res.json({
                status: 200,
                data: masterDataDetail,
            });
        } else {
            res.json({
                status: 401,
                data: { message: "No masterDataDetail" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

//add masterDataDetails
app.post("/addMasterDataDetail", async (req, res) => {
    try {
        const { categoryName, categoryId, name, Description, sortID } = req.body;
        const view = 1;
        const detailsStatus = 1;
        if (!mongoose.Types.ObjectId.isValid(categoryId)) {
            return res.status(400).json({ message: "Invalid categoryId format" });
        }

        const existingCategory = await MasterDataCategoryModel.findById(categoryId);
        if (!existingCategory) {
            return res.status(404).json({ message: "MasterDataCategory not found" });
        }

        const newMasterDataDetailsModel = new MasterDataDetailsModel({
            categoryName,
            categoryId,
            name,
            Description,
            sortID,
            detailsStatus,
            view,
        });

        const savednewMasterDataDetails = await newMasterDataDetailsModel.save();

        res.json({
            status: 200,
            data: savednewMasterDataDetails,
            message: "MasterDataDetails added successfully",
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

// Update masterDataDetails
app.put("/updateMasterDataDetails/:id", async (req, res) => {
    try {
        const {
            categoryName,
            categoryId,
            name,
            Description,
            sortID,
            detailsStatus,
        } = req.body;

        const updatedMasterDataDetails =
            await MasterDataDetailsModel.findByIdAndUpdate(
                req.params.id,
                {
                    $set: {
                        categoryName,
                        categoryId,
                        name,
                        Description,
                        sortID,
                        detailsStatus,
                    },
                },
                { new: true }
            );

        if (updatedMasterDataDetails) {
            res.json({
                status: 200,
                data: updatedMasterDataDetails,
                message: "MasterDataDetails updated successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "MasterDataDetails not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

// Delete masterDataDetails
app.put("/deleteMasterDataDetail/:id", async (req, res) => {
    try {
        const view = 0;
        const deleteMasterDataDetails =
            await MasterDataDetailsModel.findByIdAndUpdate(
                req.params.id,
                {
                    $set: {
                        view,
                    },
                },
                { new: true }
            );

        if (deleteMasterDataDetails) {
            res.json({
                status: 200,
                data: deleteMasterDataDetails,
                message: "MasterDataDetails deleted successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "MasterDataDetails not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

//add Raffles Rounds
app.post("/addRaffleRounds", async (req, res) => {
    try {
        const rafflesRound = await RaffleRoundsModal.create(req.body);

        //genarate numbers
        const batch = Math.floor(Math.random() * (9 - 1 + 1) + 1);
        const fn = batch * 100000;
        let cr = fn + 1;

        subscribedUsers = await UserModal.find({ sub_id: { $exists: true } });


        forEachAsync(subscribedUsers, async function (user) {
            if (!user.trial) {
                const Trans = await TransactionModel.findOne({ subid: user.sub_id });

                if (Trans.endfrom > Date.now()) {
                    const subscription = await functions.getSubscription(user.sub_id);
                    if (subscription) {
                        for (var i = 0; i < subscription.count; i++) {
                            await UserRafflesModel.create({
                                uid: user.uid,
                                raffleroundid: rafflesRound._id,
                                subid: user.sub_id,
                                type: "subscription",
                                createddate: Date.now(),
                                entryNumber: cr,
                            });

                            cr += 1;
                        }
                    }
                }
            }
        }).then(function () {
            if (rafflesRound) {
                rafflesRound.lastno = cr;
                rafflesRound.save();
                res.json({
                    status: 200,
                    data: rafflesRound,
                    message: "Raffles added successfully",
                });
            } else {
                res.json({
                    status: 401,
                    data: {},
                    message: "failed to add raffle",
                });
            }
        });



    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

// Update Raffles Rounds
app.put("/updateRaffleRounds/:id", async (req, res) => {
    try {
        const { name, startingtime, endtime, desc, raffleid, price, youtubeLink, drawNumbers, roundimage } = req.body;

        const updateRafflesRounds =
            await RaffleRoundsModal.findByIdAndUpdate(
                req.params.id,
                {
                    $set: {
                        name,
                        roundimage,
                        startingtime,
                        endtime,
                        desc,
                        raffleid,
                        price,
                        youtubeLink,
                        drawNumbers,
                    },
                },
                { new: true }
            );

        if (updateRafflesRounds) {
            res.json({
                status: 200,
                data: updateRafflesRounds,
                message: "Raffles updated successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "Raffles not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

// Delete Raffles Rounds
app.delete("/deleteRaffleRounds/:id", async (req, res) => {
    try {
        const status = 0;
        const deleteRafflesRounds =
            await RaffleRoundsModal.findByIdAndUpdate(
                req.params.id,
                {
                    $set: {
                        status,
                    },
                },
                { new: true }
            );

        if (deleteRafflesRounds) {
            res.json({
                status: 200,
                data: deleteRafflesRounds,
                message: "Raffles Rounds Deleted successfully",
            });
        } else {
            res.json({
                status: 404,
                data: { message: "Raffles Rounds not found" },
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: "Internal Server Error" });
    }
});

app.post('/adminLogin', async (req, res) => {
    try {
        const { email, password } = req.body;
        const isExist = await AdminModal.findOne({ email });
        if (!isExist) {
            throw Error('Email Not Exist!');
        }
        if (isExist.password !== password) {
            res.status(401).json({ message: 'Password Incorrect' });
            return;
        }
        // console.log(isExist._id);
        const token = createToken(isExist._id);
        res.status(200).json({ token: token, message: 'Login Success', admin: isExist });


    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

app.get('/getAdminUsers', LoginValidator, async (req, res) => {
    try {
        const allUsers = await AdminModal.find();
        if (allUsers) {
            res.json({
                status: 200,
                data: allUsers,
            });
        } else {
            res.json({
                status: 404,
                data: { message: "Users not found" },
            });
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
});

app.get('/getAdminUser', LoginValidator, async (req, res) => {
    try {
        const admin = await AdminModal.findById(req.query.id);
        if (admin) {
            res.json({
                status: 200,
                data: admin,
            });
        } else {
            res.json({
                status: 404,
                data: { message: "User not found" },
            });
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
});

app.put('/updateAdminUser/:id', LoginValidator, async (req, res) => {
    try {
        const admin = await AdminModal.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json({ message: 'Update success', admin });
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

app.post('/contactEmail', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        await functions.sendEmail('info@winlads.com', 'Contact Message - ' + req.body.email, `<p>Name - ${req.body.name}</p><p>Email - ${req.body.email}</p><p>Message - ${req.body.message}</p>`);
        res.status(200).json({ message: 'Email sent' });
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
});

app.get('/getRefferals', async (req, res) => {
    try {
        const uid = req.query.uid;
        const existingUser = await UserModal.findOne({ uid: uid });
        if (existingUser) {
            const refs = await UserModal.find({ refferalId: uid });

            if (refs && refs.length > 0) {
                let l1 = [];
                let l2 = [];
                let l3 = [];
                let l4 = [];

                forEachAsync(refs, async function (ref) {
                    const sub = await functions.getSubscription(ref.sub_id);
                    ref = { ...ref._doc, sub };
                    l1.push(ref);
                    const l2users = await UserModal.find({ refferalId: ref.uid });
                    await forEachAsync(l2users, async function (l2user) {
                        const sub = await functions.getSubscription(l2user.sub_id);
                        raferee = ref;
                        l2user = { ...l2user._doc, raferee, sub };
                        l2.push(l2user);
                        const l3users = await UserModal.find({ refferalId: l2user.uid });
                        await forEachAsync(l3users, async function (l3user) {
                            const sub = await functions.getSubscription(l3user.sub_id);
                            raferee = l3user;
                            l3user = { ...l3user._doc, raferee, sub };
                            l3.push(l3user);
                            const l4users = await UserModal.find({ refferalId: l3user.uid });
                            await forEachAsync(l4users, async function (l4user) {
                                const sub = await functions.getSubscription(l4user.sub_id);
                                raferee = l4user;
                                l4user = { ...l4user._doc, raferee, sub };
                                l4.push(l4user);
                            });
                        });
                    });
                }).then(async function () {
                    res.json({
                        "status": 200,
                        l1: l1,
                        l1count: l1.length,
                        l2: l2,
                        l2count: l2.length,
                        l3: l3,
                        l3count: l3.length,
                        l4: l4,
                        l4count: l4.length
                    });
                });

            } else {
                res.json({
                    "status": 200,
                    message: 'No reffarels',
                });
            }
        } else {
            res.json({
                "status": 400,
                data: null,
                message: 'No user',
            });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/forgetPassword', async (req, res) => {
    try {
        const user = await UserModal.findOne({ email: req.query.email });
        if (user) {
            const code = Math.floor(100000 + Math.random() * 900000);

            await ForgetTokenModel.deleteMany({
                uid: user.uid,
            });

            await ForgetTokenModel.create({
                uid: user.uid,
                email: user.email,
                token: code
            });

            try {
                await functions.sendEmail(user.email, 'Your Winlads Giveaway Program One-Time Password', `<div><img src="https://winlads-api.com/img/win1000ban.png"></img><p>Dear ${user.firstname},</p><p>We have received a request to generate a one-time password (OTP) for your Winlads Giveaway Program account. If you did not initiate this request, please disregard this email. 
                 <br>
                 <br>
                 Your one-time password is: ${code}
                 <br>
                 <br>
                 Please use this OTP to log in to your account and ensure the security of your account. 
 
                 <br>
                 <br>
                 If you have any questions or need further assistance, please don't hesitate to contact our support team at support@winlads.com. 
 
                 <br>
                 <br>
                 Thank you for being a part of Winlads Giveaway Program.
                    <br><br>
                 Sincerely, 
                <br>
                <img src="https://winlads-api.com/img/50win.png"></img><br>
                Winlads Team
  </p><div>`)
            } catch (error) {

            }


            res.json({
                status: 200,
                message: 'Email Sent',
            });
        } else {
            res.json({
                status: 404,
                data: { message: "User not found" },
            });
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
});

app.post('/validateToken', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const token = await ForgetTokenModel.findOne({ email: req.body.email });

        if (token.token == req.body.token) {
            res.json({
                status: 200,
                validate: true,
            });
        } else {
            res.json({
                status: 401,
                validate: false,
            });
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
});

app.post('/resetPassword', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const saltRounds = 10;
        const user = await UserModal.findOne({ email: req.body.email });
        if (user) {
            const token = await ForgetTokenModel.findOne({ uid: user.uid });

            if (token.token == req.body.token) {
                const newpass = await bcrypt.hash(req.body.password, saltRounds);
                user.password = newpass;
                user.save();

                const jwttoken = createToken(user._id);

                res.json({
                    status: 200,
                    token: jwttoken,
                    data: user,
                    message: 'Password changed',
                })
            } else {
                res.json({
                    status: 401,
                    validate: false,
                });
            }
        } else {
            res.json({
                status: 400,
                message: 'User not found',
            });
        }
    } catch (error) {
        //console.log(error);
    }
});

app.post('/applyCoupon', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const coupon = await CouponsModel.findOne({ coupen: req.body.coupen });

        if (coupon) {
            const coupenVaild = await functions.checkCoupon(req.body.uid, coupon._id);
            if (coupenVaild) {
                const counts = Array.from(
                    { length: (coupon?.count - 1) / 1 + 1 }, (value, index) => 1 + index * 1
                );
                const Round = await RaffleRoundsModal.findOne({ _id: coupon?.roundid });
                const entryNo = Round.lastno;

                forEachAsync(counts, async function (count) {

                    await UserRafflesModel.create({
                        uid: req.query.uid,
                        raffleroundid: coupon.roundid,
                        status: 1,
                        subid: "",
                        type: "coupon",
                        createddate: Date.now(),
                        entryNumber: entryNo + count
                    });
                }).then(async function () {
                    await functions.expireCoupon(req.body.uid, coupon._id);
                    Round.lastno = entryNo + counts.length;
                    Round.save();
                    res.json({
                        status: 200,
                        message: 'Coupon applied',
                    });
                });
            } else {
                res.json({
                    status: 401,
                    message: 'Coupon already applied',
                });
            }
        } else {
            res.json({
                status: 401,
                message: 'Coupon not found',
            });
        }
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

app.post('/instaFormAdd', async (req, res) => {
    if (req.method != "POST") return res.status(400);
    try {
        const user = await UserModal.findOne({ uid: req.body.uid });
        if (user) {
            const instadata = await InstaFormRequestsModel.findOne({ uid: req.body.uid });
            if (instadata) {
                res.json({
                    status: 401,
                    message: 'Already submitted',
                });
            } else {
                const insta = await InstaFormRequestsModel.create(req.body);
                if (insta) {
                    res.json({
                        status: 200,
                        data: insta
                    });
                } else {
                    res.json({
                        status: 401,
                        message: 'Error saving insta form',
                    });
                }
            }
        } else {
            res.json({
                status: 401,
                message: 'No User',
            });
        }
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
});

app.get('/delacc', async (req, res) => {
    try {
        const user = await UserModal.find({ mobile: '+' + req.query.mobile });
        await UserRafflesModel.deleteMany({ uid: user.uid });
        await UserCouponsModel.deleteMany({ uid: user.uid });
        await TransactionModel.deleteMany({ uid: user.uid });
        await BusinessCardModel.deleteMany({ uid: user.uid });
        await InstaFormRequestsModel.deleteMany({ uid: user.uid });
        await UserModal.deleteMany({ mobile: '+' + req.query.mobile });

        res.json({
            status: 200,
            msg: 'done'
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/clear', async (req, res) => {
    try {
        /* const tickets = await UserRafflesModel.find();

        forEachAsync(tickets, async function (ticket) {
            const user = await UserModal.findOne({ uid: ticket.uid });
            if (!user) {
                await UserRafflesModel.deleteOne({ _id: ticket._id });
            }
        }).then(function () {
            res.json({
                status: 200,
                msg: 'done'
            });
        }); 
        
        const coupons = await UserCouponsModel.find();

        forEachAsync(coupons, async function (coupon) {
            const user = await UserModal.findOne({ uid: coupon.uid });
            if (!user) {
                await UserCouponsModel.deleteOne({ _id: coupon._id });
            }
        }).then(function () {
            res.json({
                status: 200,
                msg: 'done'
            });
        });
        const transactions = await TransactionModel.find();

        forEachAsync(transactions, async function (transaction) {
            const user = await UserModal.findOne({ uid: transaction.uid });
            if (!user) {
                await TransactionModel.deleteOne({ _id: transaction._id });
            }
        }).then(function () {
            res.json({
                status: 200,
                msg: 'done'
            });
        });
        const cards = await BusinessCardModel.find();

        forEachAsync(cards, async function (card) {
            const user = await UserModal.findOne({ uid: card.uid });
            if (!user) {
                await BusinessCardModel.deleteOne({ _id: card._id });
            }
        }).then(function () {
            res.json({
                status: 200,
                msg: 'done'
            });
        });*/
        const cards = await InstaFormRequestsModel.find();

        forEachAsync(cards, async function (card) {
            const user = await UserModal.findOne({ uid: card.uid });
            if (!user) {
                await InstaFormRequestsModel.deleteOne({ _id: card._id });
            }
        }).then(function () {
            res.json({
                status: 200,
                msg: 'done'
            });
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.get('/rep', async (req, res) => {
    try {
        const users = await UserModal.find();
        const susers = await UserModal.find({ sub_id: { $exists: true } });

        res.json({
            status: 200,
            users: users.length,
            susers: susers
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.listen(PORT, () => {
    console.log("Server is running PORT:", PORT)
});
